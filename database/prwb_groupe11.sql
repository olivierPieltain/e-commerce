-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 17 Mai 2016 à 09:47
-- Version du serveur: 5.6.20
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `prwb_groupe11`
--
CREATE DATABASE IF NOT EXISTS `prwb_groupe11` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `prwb_groupe11`;

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(50) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(50) CHARACTER SET utf8 NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`id`, `titre`, `photo`, `enabled`) VALUES
(1, 'Bureautique', 'uploads/cat/office.png', 1),
(2, 'Graphisme', 'uploads/cat/graphisme.png', 1),
(3, 'System', 'uploads/cat/system.png', 1),
(4, 'Graphisme 3D', 'uploads/cat/3D.png', 1),
(5, 'CMS publication', 'uploads/cat/CMS.png', 1),
(6, 'Programmation', 'uploads/cat/programmation.png', 1),
(7, 'Base de donn&eacute;es', 'uploads/cat/basededonnee.png', 1),
(8, 'Edition Video Audio', 'uploads/cat/audiovideo.png', 1);

-- --------------------------------------------------------

--
-- Structure de la table `catprod`
--

CREATE TABLE IF NOT EXISTS `catprod` (
  `id_cat` int(50) NOT NULL,
  `id_prod` int(50) NOT NULL,
  PRIMARY KEY (`id_cat`,`id_prod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `catprod`
--

INSERT INTO `catprod` (`id_cat`, `id_prod`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 15),
(1, 16),
(1, 45),
(1, 50),
(1, 51),
(1, 59),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 35),
(2, 46),
(2, 52),
(2, 53),
(2, 58),
(3, 16),
(3, 17),
(3, 18),
(3, 19),
(3, 22),
(4, 20),
(4, 21),
(4, 35),
(5, 23),
(5, 24),
(5, 25),
(5, 49),
(5, 50),
(6, 23),
(6, 24),
(6, 25),
(6, 26),
(6, 27),
(6, 28),
(6, 29),
(6, 31),
(6, 32),
(6, 33),
(6, 34),
(6, 46),
(6, 47),
(6, 52),
(6, 53),
(7, 5),
(7, 6),
(7, 30),
(7, 45),
(8, 35),
(8, 36),
(8, 48);

-- --------------------------------------------------------

--
-- Structure de la table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(50) unsigned NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `firstname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `pseudo` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `phonenumber` int(12) unsigned zerofill DEFAULT NULL,
  `isAdmin` tinyint(4) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `pseudo` (`pseudo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `members`
--

INSERT INTO `members` (`id`, `lastname`, `firstname`, `birthdate`, `pseudo`, `password`, `email`, `phonenumber`, `isAdmin`, `enabled`) VALUES
(1, 'admin', 'admin', '2016-03-01', 'admin', 'c6aa01bd261e501b1fea93c41fe46dc7', 'admin@admin.net', 000001298384, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE IF NOT EXISTS `panier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idproduit` int(11) NOT NULL,
  `qteProduit` int(11) NOT NULL,
  `pseudo` varchar(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Contenu de la table `panier`
--

INSERT INTO `panier` (`id`, `idproduit`, `qteProduit`, `pseudo`) VALUES
(64, 12, 2,'admin'),
(65, 8, 1, 'admin'),
(56, 2, 1, 'admin'),
(59, 1, 1, 'admin'),
(63, 3, 1, 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(50) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `id_produit` int(50) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Contenu de la table `photos`
--

INSERT INTO `photos` (`id`, `url`, `id_produit`) VALUES
(1, 'uploads/1/23621microsoft-word-introduction-200.png', 1),
(2, 'uploads/2/546475microsoft-word-advanced-200.png', 2),
(3, 'uploads/3/830645microsoft-excel-introduction-200.png', 3),
(4, 'uploads/4/820553microsoft-excel-advanced-200.png', 4),
(5, 'uploads/5/17188microsoft-access-introduction-to-intermediate-200.png', 5),
(6, 'uploads/6/721470microsoft-access-advanced-200.png', 6),
(7, 'uploads/7/736866microsoft-powerpoint-introduction-to-advanced-200.png', 7),
(8, 'uploads/8/151667microsoft-powerpoint-introduction-to-advanced-200.png', 8),
(9, 'uploads/9/441782pics_200_outlook_introduction.png', 9),
(10, 'uploads/10/813999pics_200_outlook_intermediate.png', 10),
(11, 'uploads/17/286115introduction-to-microsoft-windows-200.png', 17),
(12, 'uploads/11/361022photoshop.gif', 11),
(15, 'uploads/12/855492photoshop_adv.gif', 12),
(16, 'uploads/13/834943indesign.gif', 13),
(17, 'uploads/14/212062illustrator.gif', 14),
(18, 'uploads/15/109534acrobate.gif', 15),
(19, 'uploads/16/594187info_init.gif', 16),
(20, 'uploads/18/708483linux.gif', 18),
(24, 'uploads/19/307242macosx.gif', 19),
(25, 'uploads/20/1728553dstudio.gif', 20),
(26, 'uploads/21/175447blender.gif', 21),
(27, 'uploads/22/254987maya.gif', 22),
(29, 'uploads/23/2188wordpress.gif', 23),
(30, 'uploads/24/272822joomla.gif', 24),
(31, 'uploads/25/223342drupal.gif', 25),
(32, 'uploads/26/808359htmlcss.gif', 26),
(33, 'uploads/27/935703javascript.gif', 27),
(34, 'uploads/28/190263php1.gif', 28),
(35, 'uploads/29/28103php2.gif', 29),
(36, 'uploads/30/69230mysql.gif', 30),
(37, 'uploads/31/602906java1.gif', 31),
(38, 'uploads/32/940856java2.gif', 32),
(39, 'uploads/33/736257csharp1.gif', 33),
(40, 'uploads/34/896893csharp2.gif', 34),
(41, 'uploads/35/221330aftereffect.gif', 35),
(42, 'uploads/1/633149microsoft-word-advanced-200.png', 1),
(43, 'uploads/1/740860wordimg1.jpg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(50) unsigned NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) CHARACTER SET utf8 NOT NULL,
  `descriptif` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prix` int(10) NOT NULL,
  `qtstock` int(30) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`id`, `libelle`, `descriptif`, `prix`, `qtstock`, `enabled`) VALUES
(1, 'Word Initiation', 'Word Initiation', 200, 1000, 1),
(2, 'Word avanc&eacute;', 'Word Avanc&eacute;', 200, 2000, 0),
(3, 'Excel Initiation', 'Excel Initiation', 200, 1000, 1),
(4, 'Excel avanc&eacute;', 'Excel Avanc&eacute;', 200, 2000, 1),
(5, 'Access Initiation', 'Access Initiation', 200, 2000, 1),
(6, 'Access avanc&eacute;', 'Access avanc&eacute;', 200, 2000, 1),
(7, 'Powerpoint initiation', 'Powerpoint initiation', 200, 2000, 1),
(8, 'Powerpoint avanc&eacute;', 'Powerpoint avanc&eacute;', 200, 2000, 1),
(9, 'Outlook initiation', 'Outlook initiation', 200, 2000, 1),
(10, 'Outlook avancé', 'Outlook avancé', 200, 2000, 1),
(11, 'Photoshop Initiation', 'Photoshop Initiation', 200, 2000, 1),
(12, 'Photoshop avanc&eacute;', 'Photoshop avanc&eacute;', 200, 2000, 1),
(13, 'InDesgin', 'InDesgin', 200, 2000, 1),
(14, 'ILLUSTRATOR', 'ILLUSTRATOR', 200, 2000, 1),
(15, 'ACROBAT PDF', 'ACROBAT PDF', 200, 2000, 1),
(16, 'Informatique Initiation', 'Informatique Initiation', 200, 2000, 1),
(17, 'Windows avanc&eacute;', 'Windows avanc&eacute;', 200, 2000, 1),
(18, 'Linux Initiation', 'Linux Initiation', 200, 2000, 1),
(19, 'MacOS', 'MacOS', 200, 2000, 1),
(20, '3D Studio MAX', '3D Studio MAX', 200, 2000, 1),
(21, 'Blender', 'Blender', 200, 2000, 1),
(22, 'MAYA', 'MAYA 3D', 200, 2000, 1),
(23, 'WORDPRESS', 'WORDPRESS', 200, 2000, 1),
(24, 'JOOMLA', 'JOOMLA', 200, 2000, 1),
(25, 'DRUPAL', 'DRUPAL', 200, 2000, 1),
(26, 'HTML / CSS', 'HTML / CSS', 200, 2000, 1),
(27, 'Javascript', 'Javascript', 200, 2000, 1),
(28, 'PHP Initiation', 'PHP Initiation', 200, 2000, 1),
(29, 'PHP avanc&eacute;', 'PHP avanc&eacute;', 200, 2000, 1),
(30, 'MYSQL', 'MYSQL', 200, 2000, 1),
(31, 'JAVA initiation', 'JAVA initiation', 200, 2000, 1),
(32, 'JAVA avanc&eacute;', 'JAVA avanc&eacute;', 200, 2000, 1),
(33, 'C# initiation', 'C# initiation', 200, 2000, 1),
(34, 'C# avanc&eacute;s', 'C# avanc&eacute;', 200, 2000, 1),
(35, 'After Effect', 'After Effect', 200, 2000, 1),
(36, 'Premiere PRO', 'Premiere PRO', 200, 2000, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

﻿<?php
require_once "framework/Model.php";

class Panier extends Model {

    public $id;
    public $pseudo;
    public $panier = array();
    public $idproduit;
    public $qteProduit;

    public function __construct($pseudo, $idproduit, $qteProduit) {

        //$panier   = new Panier("karkar", "Monproduit", 5,99);
        //($this->panier['libelleProduit'],$libelleProduit);        
        //$this->id = $id;
        $this->pseudo = $pseudo;
        $this->panier['idproduit'] = $idproduit;
        $this->panier['qteProduit'] = $qteProduit;
    }

    public static function addProduit($pseudo, $idproduit, $qteProduit) {
        //Si le panier existe
        $query = self::execute("SELECT * FROM panier where pseudo = ? and idproduit = ?", array($pseudo, $idproduit));
        $data = $query->fetch(); // un seul résultat au maximum
        //if ($query->rowCount() == 0) {

        if (empty($data)) {
            self::execute("INSERT INTO panier(pseudo, idproduit, qteProduit) VALUES(?,?,?)", array($pseudo, $idproduit, $qteProduit));
            return true;
        } else {
            self::execute("UPDATE panier SET qteProduit = ? WHERE idproduit = ? and pseudo=?", array($data['qteProduit'] + 1, $idproduit, $pseudo));
            return true;
        }
    }

    public static function removeProduits($pseudo, $idproduit) {

        self::execute("DELETE from panier WHERE idproduit=? and pseudo = ?;", array($idproduit, $pseudo));
        return true;
    }

    public static function updateQTeArticle($id, $pseudo, $qteProduit) {

        self::execute("UPDATE panier SET qteProduit=? WHERE idproduit=? and pseudo=?", array($qteProduit, $id, $pseudo));
        return true;
    }

    public static function supprimePanier($pseudo) {
        self::execute("DELETE * from panier WHERE pseudo=?;", array($panier->pseudo));
        return true;
    }

    public static function getPanier($pseudo) {
        //$query = self::execute("SELECT * from panier WHERE pseudo=?;", array($member->pseudo));        
        $query = self::execute("SELECT * from panier WHERE pseudo=?;", array($pseudo));
        $data = $query->fetchAll();

        //print_r($member);
        $results = [];
        foreach ($data as $row) {
            $results[] = new Panier($row["pseudo"], $row["idproduit"], $row["qteProduit"]);
        }
        return $results;
    }
    public static function getPanierForApi($pseudo) {
        //$query = self::execute("SELECT * from panier WHERE pseudo=?;", array($member->pseudo));        
        $query = self::execute("SELECT * from panier,produit WHERE produit.id = panier.idproduit and pseudo=?;", array($pseudo));
        $data = $query->fetchAll();

        //print_r($member);
        $results = [];
        foreach ($data as $row) {
            $results[] = array('idproduit' => $row["id"],'prix' => $row["prix"],'qteProduit' => $row["qteProduit"],'libelle' => $row["libelle"]);
        }
        return $results;
    }
    
        public static function getTotal($pseudo) {        
                
        $query = self::execute("SELECT * from panier,produit WHERE pseudo=? group by panier.id;", array($pseudo));
        $data = $query->fetchAll();
        
        $results = 0;
        foreach ($data as $row) {
            $results += $row["prix"] * $row["qteProduit"];
        }
        return $results;
    } 

}

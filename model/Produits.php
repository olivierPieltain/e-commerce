<?php

require_once "model/Member.php";
require_once "framework/Model.php";
require_once "model/Photo.php";

class Produits extends Model {

    public $id;
    public $libelle;
    public $descriptif;
    public $prix;
    public $qtstock;
    public $photos;
    public $enabled = 1;
    public $categories;

    public function __construct($id, $libelle, $descriptif, $prix, $qtstock = 0, $enabled = 1) {
        $this->id = $id;
        $this->libelle = $libelle;
        $this->descriptif = $descriptif;
        $this->prix = $prix;
        $this->qtstock = $qtstock;
        $this->enabled = $enabled;
        $this->categories = self::get_categories($id);
        $this->photos = self::get_photosByProduit($id);
    }

    public static function add_produit($produit) {
        self::execute("INSERT INTO produit(libelle,descriptif,prix,qtstock,enabled)
                       VALUES(?,?,?,?,?)", array($produit->libelle, $produit->descriptif,
            $produit->prix, $produit->qtstock, 1
        ));
        return true;
    }

    public static function add_produitbyvalues($libelle, $descriptif, $prix, $qtstock, $enabled) {
        self::execute("INSERT INTO produit(libelle,descriptif,prix,qtstock,enabled)
                       VALUES(?,?,?,?,?)", array($libelle, $descriptif,
            $prix, $qtstock, $enabled
        ));
        return true;
    }

    public static function check_product_name($libelle) {
        $query = self::execute("SELECT * FROM produit where libelle = ?", array($libelle));
        if ($query->rowCount() == 0) {
            return false;
        }
        return true;
    }

    
    public static function delete_produit($id) {
        self::execute("UPDATE produit SET enabled=0 WHERE id=?;", array($id));
        return true;
    }
    
    public static function enable_produit($id) {
        self::execute("UPDATE produit SET enabled=1 WHERE id=?;", array($id));
        return true;
    }

    public static function update_produit($produits) {
        self::execute("UPDATE produit SET libelle=?,descriptif=?,prix=?,qtstock=? ,enabled=? WHERE id=? ", array($produits->libelle, $produits->descriptif,
            $produits->prix, $produits->qtstock, $produits->enabled, $produits->id));
        return true;
    }

    public static function get_produit($id) {
        $query = self::execute("SELECT * FROM produit where id = ?", array($id));
        $data = $query->fetch(); // un seul résultat au maximum
        if ($query->rowCount() == 0) {
            return false;
        } else {
            return new Produits($data["id"], $data["libelle"], $data["descriptif"], $data["prix"], $data["qtstock"], $data["enabled"]);
        }
    }

    public static function get_produitbylibelle($libelle) {
        $query = self::execute("SELECT * FROM produit where libelle = ?", array($libelle));
        $data = $query->fetch(); // un seul résultat au maximum
        if ($query->rowCount() == 0) {
            return false;
        } else {
            return new Produits($data["id"], $data["libelle"], $data["descriptif"], $data["prix"], $data["qtstock"], $data["enabled"]);
        }
    }

        public static function get_Allproduits() {
        $query = self::execute("SELECT * FROM produit", null);
        $data = $query->fetchAll();
        $results = [];

        foreach ($data as $row) {
            $results[] = new Produits($row["id"], $row["libelle"], $row["descriptif"], $row["prix"], $row["qtstock"], $row["enabled"]);
        }
        return $results;
    }
    
    public static function get_produits() {
        $query = self::execute("SELECT * FROM produit where enabled = 1", null);
        $data = $query->fetchAll();
        $results = [];

        foreach ($data as $row) {
            $results[] = new Produits($row["id"], $row["libelle"], $row["descriptif"], $row["prix"], $row["qtstock"], $row["enabled"]);
        }
        return $results;
    }

    public static function get_categories($id) {
        $query = self::execute("SELECT * FROM category,catprod where catprod.id_cat = category.id and  id_prod =?", array($id));
        $data = $query->fetchAll();
        $results = [];

        foreach ($data as $row) {
            $results[] = new Categories($row["id"], $row["titre"], $row["photo"], $row["enabled"]);
        }
        return $results;
    }



    public static function get_produitsByCategorie($id) {
        $query = self::execute("SELECT * FROM produit,catprod where produit.enabled=1 and catprod.id_prod = produit.id and  id_cat =?", array($id));
        $data = $query->fetchAll();
        $results = [];

        foreach ($data as $row) {
            $results[] = new Produits($row["id"], $row["libelle"], $row["descriptif"], $row["prix"]);
        }
        return $results;
    }

    public static function get_photosByProduit($id) {
        $query = self::execute("SELECT * FROM photos where photos.id_produit =?", array($id));
        $data = $query->fetchAll();
        $results = [];

        foreach ($data as $row) {
            $results[] = new Photo($row["id"], $row["url"], $row["id_produit"]);
        }
        return $results;
    }

    public static function add_category($idcat, $idprod) {
        self::execute("INSERT INTO catprod(id_cat,id_prod) VALUES(?,?)", array($idcat, $idprod));
        return true;
    }

    public static function remove_AllcategoriesFromProduit($id) {
        self::execute("DELETE from catprod WHERE id_prod=?;", array($id));
        return true;
    }

}

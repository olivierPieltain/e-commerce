<?php

require_once "model/Member.php";
require_once "framework/Model.php";

class Categories extends Model {

    public $id;
    public $titre;
    public $photo;
    public $products;
    public $enabled;

    public function __construct($id, $titre, $photo, $enabled = 0) {
        $this->id = $id;
        $this->titre = $titre;
        $this->photo = $photo;
        $this->enabled = $enabled;
    }

    public static function add_categorie($categorie) {
        self::execute("INSERT INTO category(titre,photo)
                       VALUES(?,?)", array($categorie->titre, $categorie->photo
        ));
        return true;
    }

    public static function add_categoriebyvalues($titre, $photo, $enabled) {
        self::execute("INSERT INTO category(titre,photo,enabled)
                       VALUES(?,?,?)", array($titre, $photo, $enabled
        ));
        return true;
    }

    //remarque : cette méthode met en disabled le champs
    public static function delete_categorie($id) {
        self::execute("UPDATE category SET enabled=0 WHERE id=?;", array($id));
        return true;
    }

    public static function enable_categorie($id) {
        self::execute("UPDATE category SET enabled=1 WHERE id=?;", array($id));
        return true;
    }

    public static function update_categorie($categorie) {
        self::execute("UPDATE category SET titre=?,photo=?,enabled=? WHERE id=?", array($categorie->titre, $categorie->photo, $categorie->enabled, $categorie->id));
        return true;
    }

    public static function check_categorie_name($titre) {
        $query = self::execute("SELECT * FROM category where titre = ?", array($titre));
        if ($query->rowCount() == 0) {
            return false;
        }
        return true;
    }

    public static function get_categorie($id) {
        $query = self::execute("SELECT * FROM category where id = ?", array($id));
        $data = $query->fetch(); // un seul résultat au maximum
        if ($query->rowCount() == 0) {
            return false;
        } else {
            return new Categories($data["id"], $data["titre"], $data["photo"], $data["enabled"]);
        }
    }

    public static function get_categorieByTitre($titre) {
        $query = self::execute("SELECT * FROM category where titre = ?", array($titre));
        $data = $query->fetch(); // un seul résultat au maximum
        if ($query->rowCount() == 0) {
            return false;
        } else {
            return new Categories($data["id"], $data["titre"], $data["photo"], $data["enabled"]);
        }
    }

    public static function get_categories() {
        $query = self::execute("SELECT * FROM category where enabled=1", null);
        $data = $query->fetchAll();
        $results = [];

        foreach ($data as $row) {
            $results[] = new Categories($row["id"], $row["titre"], $row["photo"], $row["enabled"]);
        }
        return $results;
    }

    public static function get_Allcategories() {
        $query = self::execute("SELECT * FROM category", null);
        $data = $query->fetchAll();
        $results = [];

        foreach ($data as $row) {
            $results[] = new Categories($row["id"], $row["titre"], $row["photo"], $row["enabled"]);
        }
        return $results;
    }

    public static function get_categoriesById_prod($id_prod) {
        $query = self::execute("SELECT * FROM catprod,category where catprod.id_cat = category.id and  id_prod =?", array($id_prod));
        $data = $query->fetchAll();
        $results = [];

        foreach ($data as $row) {
            $results[] = $row["id"];
        }
        return $results;
    }

}

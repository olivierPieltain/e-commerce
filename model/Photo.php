<?php


require_once "framework/Model.php";

class Photo extends Model {

    public $id;
    public $url;
    public $id_produit;

    public function __construct($id, $url, $id_produit) {
        $this->id = $id;
        $this->url = $url;
        $this->id_produit = $id_produit;
    }

    public static function add_photo($photo) {
        self::execute("INSERT INTO photos(url,id_produit)
                       VALUES(?,?)", array($photo->url, $photo->id_produit
        ));
        return true;
    }

    //remarque : cette méthode met en disabled le champs
    public static function delete_photo($url) {
        self::execute("Delete from photos where url=?", array($url));
        return true;
    }
    
    

}

<?php

require_once "framework/Model.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Member extends Model {

    public $id;
    public $lastname;
    public $firstname;
    public $birthdate;
    public $pseudo;
    public $password;
    public $email;
    public $phonenumber;
    public $isAdmin;
    public $enabled = 1;

    public function __construct($id, $pseudo, $password, $email = null, $lastname = null, $firstname = null, $birthdate = null, $phonenumber = null, $isAdmin = 0, $enabled = 1) {
        $this->id = $id;
        $this->lastname = $lastname;
        $this->firstname = $firstname;
        $this->birthdate = $birthdate;
        $this->password = $password;
        $this->pseudo = $pseudo;
        $this->email = $email;
        $this->phonenumber = $phonenumber;
        $this->isAdmin = $isAdmin;
        $this->enabled = $enabled;
    }

    //pre : user does'nt exist yet
    public static function add_member($member) {
        self::execute("INSERT INTO Members(pseudo,password,email,lastname,firstname,birthdate, phonenumber)
                       VALUES(?,?,?,?,?,?,?)", array($member->pseudo, $member->password,
            $member->email, $member->lastname, $member->firstname, $member->birthdate, $member->phonenumber
        ));
        return true;
    }

    public static function check_hashedPassword($hashedPassword) {
        $query = self::execute("SELECT * FROM Members where password = ?", array($hashedPassword));
        if ($query->rowCount() == 0) {
            return false;
        }
        return true;
    }

    public static function check_pseudo($pseudo) {
        $query = self::execute("SELECT * FROM Members where pseudo = ?", array($pseudo));
        if ($query->rowCount() == 0) {
            return false;
        }
        return true;
    }

    public static function check_password($password) {
        $query = self::execute("SELECT * FROM Members where password = ?", array($password));
        if ($query->rowCount() == 0) {
            return false;
        }
        return true;
    }

    public static function check_pseudo_with_id($id, $pseudo) {
        $query = self::execute("SELECT * FROM Members where id = ? and pseudo = ?", array($id, $pseudo));
        if ($query->rowCount() == 0) {
            return false;
        }
        return true;
    }

    public static function check_email($email) {
        $query = self::execute("SELECT * FROM Members where email = ?", array($email));
        if ($query->rowCount() == 0) {
            return false;
        }
        return true;
    }

    //remarque : cette méthode met en disabled le champs
    public static function delete_member($id) {
        self::execute("UPDATE Members SET enabled=0 WHERE id=?;", array($id));
        return true;
    }

    public static function enable_member($id) {
        self::execute("UPDATE Members SET enabled=1 WHERE id=?;", array($id));
        return true;
    }

    public static function update_member($member) {
        self::execute("UPDATE Members SET pseudo=?,password=?,email=?,lastname=?,firstname=?,birthdate=?, phonenumber=?, enabled=?,isAdmin=? WHERE pseudo=? ", array($member->pseudo, $member->password,
            $member->email, $member->lastname, $member->firstname, $member->birthdate, $member->phonenumber, $member->enabled, $member->isAdmin, $member->pseudo));
        return true;
    }

    public static function get_member($id) {
        $query = self::execute("SELECT * FROM Members where id = ?", array($id));
        $data = $query->fetch(); // un seul résultat au maximum
        if ($query->rowCount() == 0) {
            return false;
        } else {
            return new Member($data["id"], $data["pseudo"], $data["password"], $data["email"], $data["lastname"], $data["firstname"], $data["birthdate"], $data["phonenumber"], $data["isAdmin"], $data["enabled"]);
        }
    }

    public static function get_member_by_pseudo($pseudo) {
        $query = self::execute("SELECT * FROM Members where pseudo = ?", array($pseudo));
        $data = $query->fetch(); // un seul résultat au maximum
        if ($query->rowCount() == 0) {
            return false;
        } else {
            return new Member($data["id"], $data["pseudo"], $data["password"], $data["email"], $data["lastname"], $data["firstname"], $data["birthdate"], $data["phonenumber"], $data["isAdmin"], $data["enabled"]);
        }
    }

    public static function get_members() {
        $query = self::execute("SELECT * FROM members", null);
        $data = $query->fetchAll();
        $results = [];
        foreach ($data as $row) {
            $tempMember;
            $tempMember = new Member($row["id"], $row["pseudo"], $row["password"], $row["email"], $row["lastname"], $row["firstname"], $row["birthdate"], $row["phonenumber"], $row["isAdmin"], $row["enabled"]);
            $tempMember->id = $row['id'];
            $results[] = $tempMember;
        }

        return $results;
    }

}

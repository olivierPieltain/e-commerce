<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Log In</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
        <script src="lib/jquery-2.2.0.min.js" type="text/javascript"></script>
        <script src="lib/toastr.js" type="text/javascript"></script>
        <link rel="stylesheet" href="lib/toastr.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />

        <script>
            $(function () {
                $("input:text:first").focus();
            });
        </script>
    </head>
    <body>

        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>               
                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>




  <br/><br />
        <div class="row"> 

            <div class="medium-4  small-up-4 large-up-4 small-centered columns"> 
                <h4 class="title">Log In</h4>

                <form id="target"  action="login/login" method="post">
                    <table>
                        <tr>
                            <td>Pseudo:</td>
                            <td><input id="pseudo" name="pseudo" type="text" value="<?= $pseudo ?>" ></td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td><input id="password" name="password" type="password" value="<?= $password ?>" ></td>
                        </tr>
                    </table>
                    <input class="button"  type="submit" style="float:left;" value="Log In">
                </form>
                <a href="login/passwordRetrieve" name="passwordRetrieve" class="button"  style="float:right;" type="button"> MDP encore oublié? </a>

                </br> </br>   </br>

                user : admin 
                </br>
                pass : admin

                <?php if ($error): ?>
                    <div class='errors'><br><br><?= $error ?></div>
                    <?php endif; ?>
            </div>
        </div>
        <script src="lib/foundation-6/js/vendor/jquery.min.js"></script>
        <script src="lib/foundation-6/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>

        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/dist/additional-methods.min.js"></script>
        <script>
            $("#target").validate({
                rules: {
                    pseudo: {
                        required: true,
                        minlength: 3,
                    },
                    password: {
                        required: true,
                        minlength: 3,
                        remote: {
                            url: "login/valid_password",
                            type: "post"
                        }
                    }
                },
                messages: {
                    password: {
                        remote: jQuery.validator.format('Ce mot de passe n\'est pas reconnu')
                    }
                }
            });

        </script>




    </body>
</html>

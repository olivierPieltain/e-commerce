<!DOCTYPE html>
<html >
    <head>
        <meta charset="UTF-8">
        <title>Membres</title>
        <base href="<?= $web_root ?>"/>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="lib/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />        





    </head>
    <body>



        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>                   

                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>





        <h4 class="title">Membres</h4>  



        <div class="row">
            <div class="medium-8  small-up-4 large-up-6 ">


                <table id="membersTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th >Pseudo</th>
                            <th >Lastname</th>
                            <th>Firstname</th>
                            <th>birthdate</th>
                            <th>email</th>
                            <th>phonenumber</th>
                            <th><div style="width: 150px;" ></div></th>

                    </tr>
                    </thead>
                    <tbody>

                        <?php
                        if ($member->isAdmin) {
                            foreach ($members as $val):
                                //	 		isAdmin 	enabled 
                                echo "<tr>";
                                echo "<td>" . $val->id . "</td>";
                                echo "<td>" . $val->pseudo . "</td>";
                                echo "<td>" . $val->lastname . "</td>";
                                echo "<td>" . $val->firstname . "</td>";
                                echo "<td>" . $val->birthdate . "</td>";
                                echo "<td>" . $val->email . "</td>";
                                echo "<td>" . $val->phonenumber . "</td>";


                                //echo "<td><a href='member/edit_users/$val->pseudo' > Edit</a></td>";
                                echo "<td>";
                                echo "<a href='member/edit_users/$val->id' > <img src='view/img/edit.png' width='20'/></a>";
                                echo " : ";
                                echo "<a href='member/delete_users/$val->id' > <img src='view/img/delete.png' width='20'/></a>";


                                if ($val->enabled == 0) {
                                    echo " : ";
                                    echo "<a href='member/enable_users/$val->id' > <img src='view/img/desable.png' title='Restaurer l\'utilisateur' width='20'/></a>";
                                }

                                if ($val->isAdmin == 1) {
                                    echo " : <img src='view/img/admin.png' />";
                                }


                                echo "</td>";


                                echo "</tr>";

                            endforeach;
                        } else {
                            echo "you aren't Administrator !";
                        }
                        ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th >Pseudo</th>
                            <th >Lastname</th>
                            <th>Firstname</th>
                            <th>birthdate</th>
                            <th>email</th>
                            <th>phonenumber</th>
                            <th> </th>

                        </tr>
                    </tfoot>

                </table>

                <br>
                <br>

            </div>
        </div>


        <script src="lib/jquery-2.2.0.min.js"></script> 
        <script type="text/javascript" src="lib/DataTables/datatables.min.js"></script>
        <script src="lib/foundation-6/js/foundation.min.js"></script>

        <script>
            $(document).ready(function () {
                $("#membersTable").DataTable();
            });
            $(document).foundation();
        </script>

    </body>        

</html>
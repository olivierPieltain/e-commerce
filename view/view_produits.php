<!DOCTYPE html>
<html >
    <head>
        <meta charset="UTF-8">
        <title>Produits</title>
        <base href="<?= $web_root ?>"/>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="lib/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />
              
        <style>
            button.accordion {
                background-color: #eee;
                color: #444;
                cursor: pointer;
                padding: 18px;
                width: 100%;
                border: none;
                text-align: left;
                outline: none;
                font-size: 15px;
                transition: 0.4s;
            }

            button.accordion.active, button.accordion:hover {
                background-color: #ddd;
            }

            div.panel {
                padding: 0 18px;
                background-color: white;
                max-height: 0;
                overflow: hidden;
                transition: 0.6s ease-in-out;
                opacity: 0;
            }

            div.panel.show {
                opacity: 1;
                max-height: 500px;  
            }
        </style>





    </head>
    <body>



        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>                     

                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>





        <h4 class="title">Produits</h4>  


        <button class="accordion">Recherche Multi-critères</button>
        <div id="foo" class="panel">


            <table cellpadding="3" cellspacing="0" border="0" >
                <thead>
                    <tr>
                        <th>Target</th>
                        <th>Search text</th>
                    </tr>
                </thead>
                <tbody>

                    <tr id="filter_col2" data-column="1">
                        <td>Libelle</td>
                        <td align="center"><input type="text" class="column_filter" id="col1_filter"></td>

                    </tr>
                    <tr id="filter_col3" data-column="2">
                        <td>Descriptif</td>
                        <td align="center"><input type="text" class="column_filter" id="col2_filter"></td>

                    </tr>
                    <tr>
                        <td>Minimum price:</td>
                        <td><input type="text" id="min" name="min"></td>
                    </tr>
                    <tr>
                        <td>Maximum price:</td>
                        <td><input type="text" id="max" name="max"></td>
                    </tr>
                    <tr id="filter_col5" data-column="4">
                        <td>Categorie</td>
                        <td align="center"><input type="text" class="column_filter" id="col4_filter"></td>

                    </tr>

                </tbody>
            </table>
        </div>
        <div class="inner">
            <table id="productTable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th >Libelle</th>
                        <th>Descriptif</th>
                        <th>Prix</th>
                        <th>Categories</th>
                        <th></th>

                    </tr>
                </thead>
                <tbody>

                    <?php
                    foreach ($produits as $val):
                        echo "<tr>";
                        echo "<td>" . $val->id . "</td>";
                        echo "<td>" . $val->libelle . "</td>";
                        echo "<td>" . $val->descriptif . "</td>";
                        echo "<td>" . $val->prix . "</td>";
                        echo "<td>";
                        foreach ($val->categories as $cat):
                            echo $cat->titre . '  ';
                        endforeach;
                        echo "</td>";





                        if ($member->isAdmin) {
                            echo "<td>";
                            echo "<a href='produits/edit_produit/$val->id' > <img src='view/img/edit.png' title='Editer le produit' width='20'/></a>";
                            echo " : ";
                            echo "<a href='produits/delete_produit/$val->id' > <img src='view/img/delete.png' title='Supprimer le produit' width='20'/></a>";
                            echo " : ";
                            echo "<a href='produits/add_photos/$val->id' > <img src='view/img/appareil_photo.png' title='Editer les photos de produits' width='20'/></a>";
                            if ($val->enabled == 0) {
                                echo " : ";
                                echo "<a href='produits/enable_produit/$val->id' > <img src='view/img/desable.png' title='Restaurer le produit' width='20'/></a>";
                            }
                            echo "</td>";
                        } else {
                            echo "<td></td>";
                        }

                        echo "</tr>";

                    endforeach;
                    ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th >Libelle</th>
                        <th>Descriptif</th>
                        <th>Prix</th>
                        <th>Categories</th>
                        <th></th>

                    </tr>
                </tfoot>

            </table>
        </div>
        <br>
        <br>

 



<script src="lib/jquery-2.2.0.min.js"></script>  


        
        <script src="lib/foundation-6/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function () {
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
        }
    }
</script>
<script type="text/javascript" src="lib/DataTables/datatables.min.js"></script>
<script>

    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = parseInt($('#min').val(), 10);
                var max = parseInt($('#max').val(), 10);
                var price = parseFloat(data[3]) || 0; // use data for the age column

                if ((isNaN(min) && isNaN(max)) ||
                        (isNaN(min) && price <= max) ||
                        (min <= price && isNaN(max)) ||
                        (min <= price && price <= max))
                {
                    return true;
                }
                return false;
            }
    );
    function filterGlobal() {
        $('#productTable').DataTable().search(
                $('#global_filter').val(),
                $('#global_regex').prop('checked'),
                $('#global_smart').prop('checked')
                ).draw();
    }

    function filterColumn(i) {
        $('#productTable').DataTable().column(i).search(
                $('#col' + i + '_filter').val(),
                $('#col' + i + '_regex').prop('checked'),
                $('#col' + i + '_smart').prop('checked')
                ).draw();
    }

    $(document).ready(function () {
        $('#productTable').DataTable();

        $('input.global_filter').on('keyup click', function () {
            filterGlobal();
        });

        $('input.column_filter').on('keyup click', function () {
            filterColumn($(this).parents('tr').attr('data-column'));
        });
        $('#min, #max').keyup(function () {
            table.draw();
        });
    });

</script>

</body>        

</html>
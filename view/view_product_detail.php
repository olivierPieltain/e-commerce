<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <base href="<?= $web_root ?>"/>
        <title>Welcome to ITConsulting</title>

        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />

        <link rel="stylesheet" type="text/css" href="lib/DataTables/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" href="lib/toastr.css"/>
        <link rel="stylesheet" type="text/css" href="lib/index.css"/>



    </head>
    <body>

        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>                   
                    <li><a href="produits">Rechercher</a></li> 
                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>





        <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
            <ul class="orbit-container">
                <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span>&#9664;</button>
                <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span>&#9654;</button>


                <li class="orbit-slide is-active">
                    <img src="view/img/banner3.png">
                </li>
                <li class="orbit-slide">
                    <img src="view/img/banner1.png">
                </li>
                <li class="orbit-slide">
                    <img src="view/img/banner2.png">
                </li>

            </ul>
        </div>









        <div class="row column text-center">
            <h3>Nos Categories de cours en pdf</h3>
            <hr>
        </div>

        <div class="row text-left lightBkg">



            <a href="" >Toutes nos catégorie.</a>
            </br></br>
            <?php
            foreach ($categories as $val):

                echo '<a  href="home/indexByCat/' . $val->id . '">';
                echo '<img class="thumbnail"  width="70" hspace="5"  title="' . $val->titre . '"' . '   src="' . $val->photo . '">';
                echo '</a>';
            endforeach;
            ?>




        </div>
        <hr>


        <div class="row"> 

            <?= $produit->libelle; ?> <br>
            Descriptif : <?= $produit->descriptif; ?> <br>
            Prix :  <?= $produit->prix; ?> €<br>
            Quantité en stock : <?= $produit->qtstock; ?> <br>

            <br />
            <?php
            echo '<a id="' . $produit->id . '"  class="button round" href="panier/ajouterArticle/' . $produit->id . '">Ajouter au panier</a>';
            ?>
            <div  > 

                <div id="jssor_1" >
                    <!-- Loading Screen -->
                    <div data-u="loading" id="jloading">
                        <div id="jssor_1filter"></div>
                        <div id="jssor_position"></div>
                    </div>
                    <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 800px; height: 356px; overflow: hidden;">

                        <?php
                        foreach ($produit->photos as $picture):
                            echo '<div data-p="144.50" style="display: none;">';
                            echo '<img data-u="image" src=" ' . $picture->url . ' " alt="RSS"  />';
                            echo '<img data-u="thumb" src=" ' . $picture->url . ' " alt="RSS"  />';
                            echo '</div>';



                        endforeach;
                        ?>

                    </div>
                    <!-- Thumbnail Navigator -->
                    <div data-u="thumbnavigator" class="jssort01" >
                        <!-- Thumbnail Item Skin Begin -->
                        <div data-u="slides" class="jssort01">
                            <div data-u="prototype" class="p">
                                <div class="w">
                                    <div data-u="thumbnailtemplate" class="t"></div>
                                </div>
                                <div class="c"></div>
                            </div>
                        </div>
                        <!-- Thumbnail Item Skin End -->
                    </div>
                    <!-- Arrow Navigator -->
                    <span data-u="arrowleft" class="jssora05l" style="top:158px;left:128px;width:40px;height:40px;"></span>
                    <span data-u="arrowright" class="jssora05r" style="top:158px;right:128px;width:40px;height:40px;"></span>

                </div>


            </div>

        </div>



        <br>
        <br>


        <div class="large-12  text-center">   
            <h3>Nos supports de cours</h3>
        </div> 

        <hr>


        <div class="row">

            <div class="medium-8  small-up-2 large-up-4  text-center column">


                <?php
                foreach ($produits as $prod):
                    echo '<div  class="column" >';
                    if (array_key_exists(0, $prod->photos)) {
                        echo '<a class="thumbnail" href="produits/product_details/' . $prod->id . '"><img style="width: 100px; height:100px;"  src="' . $prod->photos[0]->url . '" alt="RSS"  /></a>';
                    } else {
                        echo '<a class="thumbnail" href="produits/product_details/' . $prod->id . '"><img style="width: 100px; height:100px;"  src="uploads/prod/pdf.png" alt="RSS"  /></a>';
                    }
                    echo '<p>' . $prod->libelle . '</p>';
                    echo '<p>€ ' . $prod->prix . '</p>';
                    echo '<a id="' . $prod->id . '"  class="button round" href="panier/ajouterArticle/' . $prod->id . '">Ajouter au panier</a>';

                    echo '</div>';
                endforeach;
                ?>


            </div>



            <div class="large-3 column text-center" >


                <div id="monPanier">


                </div>







            </div>

            <hr>



            <div class="row column">
                <div class="callout primary text-center">
                    <h3>-10% pour les fêtes de Carnaval !</h3>
                </div>
            </div>
            <hr>




            <div class="row">
                <div class="medium-8  small-up-2 large-up-4  text-center column  callout secondary">


                    <?php
                    echo '<h4>Top 5 des produits</h4>';
                    echo '<br>';
                    $count = 0;


                    foreach ($produits as $prod):
                        echo '<div  class="column" >';
                        if (array_key_exists(0, $prod->photos)) {
                            echo '<a class="thumbnail" href="produits/product_details/' . $prod->id . '"><img style="width: 100px; height:100px;"  src="' . $prod->photos[0]->url . '" alt="RSS"  /></a>';
                        } else {
                            echo '<a class="thumbnail" href="produits/product_details/' . $prod->id . '"><img style="width: 100px; height:100px;"  src="uploads/prod/pdf.png" alt="RSS"  /></a>';
                        }
                        echo '<p>' . $prod->libelle . '</p>';
                        echo '<p>€ ' . $prod->prix . '</p>';
                        echo '<a id="' . $prod->id . '"  class="button round" href="panier/ajouterArticle/' . $prod->id . '">Ajouter au panier</a>';

                        echo '</div>';

                        $count++;
                        if ($count == 5)
                            break;
                    endforeach;
                    ?>


                </div>
            </div>


        </div>   


        <!-- #region Jssor Slider Begin -->

        <!-- Generated by Jssor Slider Maker. -->
        <!-- This demo works with jquery library -->

        <script src="lib/jquery-2.2.0.min.js"></script>
        <script type="text/javascript" src="lib/js/jssor.slider.mini.js"></script>
        <!-- use jssor.slider.debug.js instead for debug -->
        <script>
            jQuery(document).ready(function($) {

                var jssor_1_SlideshowTransitions = [
                    {$Duration: 1200, x: 0.3, $During: {$Left: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, x: -0.3, $SlideOut: true, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, x: -0.3, $During: {$Left: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, x: 0.3, $SlideOut: true, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, y: 0.3, $During: {$Top: [0.3, 0.7]}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, y: -0.3, $SlideOut: true, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, y: -0.3, $During: {$Top: [0.3, 0.7]}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, y: 0.3, $SlideOut: true, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, x: 0.3, $Cols: 2, $During: {$Left: [0.3, 0.7]}, $ChessMode: {$Column: 3}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, x: 0.3, $Cols: 2, $SlideOut: true, $ChessMode: {$Column: 3}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, y: 0.3, $Rows: 2, $During: {$Top: [0.3, 0.7]}, $ChessMode: {$Row: 12}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, y: 0.3, $Rows: 2, $SlideOut: true, $ChessMode: {$Row: 12}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, y: 0.3, $Cols: 2, $During: {$Top: [0.3, 0.7]}, $ChessMode: {$Column: 12}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, y: -0.3, $Cols: 2, $SlideOut: true, $ChessMode: {$Column: 12}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, x: 0.3, $Rows: 2, $During: {$Left: [0.3, 0.7]}, $ChessMode: {$Row: 3}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, x: -0.3, $Rows: 2, $SlideOut: true, $ChessMode: {$Row: 3}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, x: 0.3, y: 0.3, $Cols: 2, $Rows: 2, $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]}, $ChessMode: {$Column: 3, $Row: 12}, $Easing: {$Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, x: 0.3, y: 0.3, $Cols: 2, $Rows: 2, $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]}, $SlideOut: true, $ChessMode: {$Column: 3, $Row: 12}, $Easing: {$Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, $Delay: 20, $Clip: 3, $Assembly: 260, $Easing: {$Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, $Delay: 20, $Clip: 3, $SlideOut: true, $Assembly: 260, $Easing: {$Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, $Delay: 20, $Clip: 12, $Assembly: 260, $Easing: {$Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
                    {$Duration: 1200, $Delay: 20, $Clip: 12, $SlideOut: true, $Assembly: 260, $Easing: {$Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2}
                ];

                var jssor_1_options = {
                    $AutoPlay: true,
                    $FillMode: 1,
                    $SlideshowOptions: {
                        $Class: $JssorSlideshowRunner$,
                        $Transitions: jssor_1_SlideshowTransitions,
                        $TransitionsOrder: 1
                    },
                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$
                    },
                    $ThumbnailNavigatorOptions: {
                        $Class: $JssorThumbnailNavigator$,
                        $Cols: 10,
                        $SpacingX: 8,
                        $SpacingY: 8,
                        $Align: 360
                    }
                };

                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                //responsive code begin
                //you can remove responsive code if you don't want the slider scales while window resizing
                function ScaleSlider() {
                    var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                    if (refSize) {
                        refSize = Math.min(refSize, 400);
                        jssor_1_slider.$ScaleWidth(refSize);
                    } else {
                        window.setTimeout(ScaleSlider, 3);
                    }
                }
                ScaleSlider();
                $(window).bind("load", ScaleSlider);
                $(window).bind("resize", ScaleSlider);
                $(window).bind("orientationchange", ScaleSlider);
                //responsive code end
            });
        </script>






        <script src="lib/foundation-6/js/vendor/jquery.min.js"></script>
        <script src="lib/foundation-6/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
        <script>

            function generateTable() {
                $.ajax({
                    url: 'panier/getData',
                    data: "",
                    dataType: 'json',
                    success: function(data) {

                        //thead
                        var tablearea = document.getElementById('monPanier');
                        table = document.createElement('table');
                        table.id = "basket";
                        table.style = "width: 400px";
                        var header = table.createTHead();

                        var header = header.insertRow(0);
                        header.insertCell(0);
                        var headerlibelle = header.insertCell(0);

                        var headerquantite = header.insertCell(0);
                        var headerprixUnitaire = header.insertCell(0);

                        headerlibelle.innerHTML = "<b>Prix </b>";
                        headerquantite.innerHTML = "<b>Quantite</b>";
                        headerprixUnitaire.innerHTML = "<b>Libelle</b>";
                        headerprixUnitaire.style.textAlign = "center";
                        headerquantite.style.textAlign = "center";
                        headerlibelle.style.textAlign = "center";

                        //thead
                        //tbody
                        var tBody = document.createElement("tbody");
                        table.appendChild(tBody);
                        for (var i = 0; i < data.length; i++) {


                            var prix = JSON.stringify(data[i].prix);
                            var idprod = JSON.stringify(data[i].idproduit);
                            var libelle = JSON.stringify(data[i].libelle);
                            var qteProduit = JSON.stringify(data[i].qteProduit);
                            var tr = document.createElement('tr');
                            tr.id = "lignePanier";
                            tBody.appendChild(tr);
                            tr.appendChild(document.createElement('td'));
                            tr.appendChild(document.createElement('td'));
                            tr.appendChild(document.createElement('td'));
                            tr.appendChild(document.createElement('td'));

                            tr.cells[0].appendChild(document.createTextNode(libelle.replace(/"/g, " ")));
                            var input = document.createElement('input');
                            tr.cells[1].appendChild(input);
                            input.id = data[i].idproduit;
                            input.type = "text";
                            input.name = "quantity";
                            input.className = "quantity";
                            input.value = qteProduit.replace(/"/g, " ");
                            input.style.width = "50px";
                            tr.cells[2].appendChild(document.createTextNode(prix.replace(/"/g, " ")));
                            var supprimer = document.createElement('a');
                            var poubelle = document.createElement('IMG');
                            poubelle.src = "view/img/delete.png";
                            poubelle.title = "supprimer";
                            supprimer.appendChild(poubelle);
                            supprimer.name = "supprimer";
                            supprimer.className = "supprimer";
                            poubelle.width = 30;

                            supprimer.id = data[i].idproduit;
                            supprimer.href = "panier/supprimerArticle/" + idprod.replace(/"/g, "");
                            tr.cells[3].appendChild(supprimer);


                            table.appendChild(tr);

                        }
                        //tbody
                        var footer = table.createTFoot();
                        var row = footer.insertRow(0);
                        var cell3 = row.insertCell(0);

                        var cell1 = row.insertCell(0);

                        cell3.id = "total";

                        cell1.innerHTML = "Total";
                        cell3.style = "text-align:center;border:0px;";
                        cell1.style = "text-align:center;border:0px;";
                        var cell2 = row.insertCell(1);
                        var cell4 = row.insertCell(3);


                        tablearea.appendChild(table);





                        $(".supprimer").click(function(event) {
                            event.preventDefault();
                            var idprod = $(this).attr('id');
                            $.ajax({
                                url: 'panier/supprimerArticle',
                                method: "POST",
                                data: {idprod: idprod},
                                dataType: 'json',
                                complete: function() {
                                    $("#monPanier").empty();
                                    generateTable();
                                    getTotalPrice();
                                    toastr["success"]("Product deleted");
                                }

                            });

                        });
                        $(".quantity").change(function() {
                            var qty = $(this).val();
                            var idprod = parseInt($(this).attr('id'));
                            $.ajax({
                                url: 'panier/updateQTeArticle',
                                method: "POST",
                                data: {idprod: idprod, qty: qty},
                                dataType: 'json',
                                complete: function() {
                                    getTotalPrice();
                                    toastr["success"]("Quantity successfully updated");
                                }

                            });

                        });
                    }
                });
            }
            $(".button").click(function(event) {
                event.preventDefault();
                sessionCheck();
                var idprod = $(this).attr('id');
                $.ajax({
                    url: 'panier/ajouterArticle',
                    method: "POST",
                    data: {idprod: idprod},
                    dataType: 'json',
                    complete: function() {

                        $("#monPanier").empty();
                        generateTable();
                        getTotalPrice();
                    }
                });




            });
            $(document).ready(function() {
                generateTable();
                getTotalPrice();


            });
            function getTotalPrice() {
                $.ajax({
                    url: 'panier/getPrice',
                    data: "",
                    dataType: 'json',
                    success: function(data) {

                        document.getElementById("total").innerHTML = data;


                    }
                });
            }
            function sessionCheck() {

                $.ajax({
                    url: 'panier/sessionCheck',
                    data: "",
                    dataType: 'json',
                    success: function(data) {
                        $("#panier").html('Panier');
                        toastr["success"]("Product added");
                    },
                    error: function(data) {
                        toastr["error"]('Connectez-vous pour ajouter au panier');
                    }



                });

            }




        </script>

    </body>
    <script src="lib/toastr.js"></script>
</html>

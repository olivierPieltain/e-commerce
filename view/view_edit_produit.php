<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Edit produit</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />
        <link rel="stylesheet" type="text/css" href="lib/index.css"/>

    </head>
    <body>

        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>          
                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>




        <div class="row"> 

            
            <?php if (count($produit->id) == 0): ?>
                pas d'id spécifié !
            <?php else: ?>

                <div class="row">
                    <form id="formEditProduit" method='post' action='produits/update_produit/<?= $produit->id; ?>' >                   


                        ID : <?= $produit->id; ?><br>
                        ID : <input type="hidden" name="id" value = "<?= $produit->id; ?>" />
                        Libellé : <input type="text" name="libelle" value = "<?= $produit->libelle; ?>" />
                        Descriptif : <input type="text" name="descriptif" value = "<?= $produit->descriptif; ?>" />
                        Prix : <input type="text" name="prix" value = "<?= $produit->prix; ?>" />
                        Quantité en stock : <input type="text" name="qtstock" value = "<?= $produit->qtstock; ?>" />
                        Enabled : <input type="checkbox" name="enabled" <?= ($produit->enabled == 1) ? "CHECKED" : ""; ?>/>


                        <p>Choix de la Catégorie:</p>   

                        <?php ?>
                        <div class="row">

                            <?php
                            foreach ($categorylist as $category) {
                                $check = "";
                                if (in_array($category->id, $catprod)) {
                                    $check = "CHECKED";
                                }
                                echo '<div class="small-6 medium-4 large-3 columns" ><input type="checkbox" ' . $check . ' name="categoryName[]"  value="' . $category->id . '"  /> ' . $category->titre . '</div>';
                            }
                            ?>

                        </div>

                        <input type='submit' value='Save Produit'>
                    </form>
                </div>
                <?php if (strlen($success) != 0): ?>
                    <p><span class='success'><?= $success ?></span></p>
                <?php endif; ?>

                <?php if (strlen($error) != 0): ?>
                    <p><span class='errors'><?= $error ?></span></p>
                    <?php endif; ?>  

            <?php endif; ?>            
            <br>
            <br>


        </div>



        <script src="lib/jquery-2.2.0.min.js"></script>         
        <script src="lib/foundation-6/js/foundation.min.js"></script>


        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/additional-methods.min.js"></script>
        <script>

            $("#formEditProduit").validate({
                rules: {
                    libelle: {
                        required: true,
                        minlength: 3
                    },
                    descriptif: {
                        required: true,
                        minlength: 5
                    },
                    prix: {
                        required: true,
                        min: 0
                    },
                    qtstock: {
                        required: true,
                        min: 0
                    }

                }
            });
            $(document).foundation();
        </script>


    </body>
</html>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Define password</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
        <script src="lib/jquery-2.2.0.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />
        <link rel="stylesheet" type="text/css" href="lib/toastr.css"/>

    </head>
    <body>

        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>              
                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>



        <h4>Password defining</h4>

        <div class="main">
            Enter your new password :
            <br><br>

            <div class="row"> 
                <div class="medium-4  small-up-4 large-up-4 "> 
                    <form id="passwordForm" action="login/password_changing" method="post" >
                        <input id="id" name="id" type="hidden" size="16"  value="<?= $member->id; ?>" >
                        <table>
                            <tr>
                                <td>Pseudo:</td>
                                <td><input id="pseudo" name="pseudo" type="text" size="16"  value="" ></td>

                            </tr>

                            <tr>
                                <td>Password:</td>
                                <td><input id="password" name="password" type="password" size="16"  value="" ></td>

                            </tr>
                            <tr>
                                <td>Confirm Password:</td>
                                <td><input id="password_confirm" name="password_confirm" size="16" type="password" value="" ></td>

                            </tr>
                        </table>
                        <input class="button" id="btn" type="submit" value="Validate">
                    </form>

                </div>
            </div>

        </div>

        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/additional-methods.min.js"></script>
        <script>

            $("#passwordForm").validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 3
                    },
                    password_confirm: {
                        required: true,
                        minlength: 3,
                        equalTo: "#password"
                    }
                }
            });

        </script>
        <script>
            $(".button").click(function (event) {
                event.preventDefault();
                var id = $("#id").val();
                var pseudo = $("#pseudo").val();
                var password = $("#password").val();
                var password_confirm = $("#password_confirm").val();
                $.ajax({
                    url: 'login/password_changing',
                    method: "POST",
                    data: {id: id, pseudo: pseudo, password: password, password_confirm: password_confirm},
                    dataType: 'JSON',
                    success: function (msg) {
                        if (msg == "ok") {
                            toastr["success"]("Password changed, you can now log in with your new password.");
                            $("#password").val("");
                            $("#password_confirm").val("");
                        } else if (msg == "ko") {
                            toastr["error"]("No match.");
                        }

                    }
                });

            });
        </script>

        <script src="lib/foundation-6/js/vendor/jquery.min.js"></script>
        <script src="lib/foundation-6/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
        <script src="lib/toastr.js"></script>
    </body>
</html>
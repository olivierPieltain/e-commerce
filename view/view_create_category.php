<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Produits</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />

    </head>
    <body>



        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>                  

                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>

        <h4 class="title">Create a new Category</h4>   



        <div class="row">
            <div class="medium-3  small-up-3 large-up-3 "> 






                <form id="formCreateCategory" method='post' action='categories/create_category' enctype='multipart/form-data' >                   


                    Titre : <input type="text" id="titre" name="titre" value = ""  />
                    Photo : <input type="file" id="image" name="image" class='image' value = "" />



                    </br>
                    <input id="submit" type='submit' value='Save Category'>
                </form>
                </br>
                <form id="createCat" method="post"  action="produits/create_produit">
                    <input value="Get Back to product creation" type="submit" />
                </form>     





                <br>
                <br>

            </div>
        </div>

        <script src="lib/foundation-6/js/vendor/jquery.min.js"></script>
        


        <script src="lib/jquery-2.2.0.min.js" type="text/javascript"></script>
        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/additional-methods.min.js"></script>
        <script src="lib/toastr.js"></script>
               <script src="lib/foundation-6/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
        <script>


            $("#formCreateCategory").validate({
                rules: {
                    titre: {
                        required: true,
                        minlength: 3,
                        remote: {
                            url: "categories/valid_categorie",
                            type: "post"
                        }
                    },
                    photo: {
                        required: true,
                        url: true
                    }
                },
                messages: {
                    titre: {
                        remote: jQuery.validator.format('Cette categorie existe deja')
                    }
                }
            });
                    < script >
                                $(document).foundation();
        </script>

    </script>
</body>
</html>
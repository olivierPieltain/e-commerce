<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sign Up</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
        <script src="lib/jquery-2.2.0.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />

    </head>
    <body>

        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>                     
                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>


        <br><br>
        <h4>Sign Up</h4>

        <div class="main">


            <div class="row"> 

                <div class="medium-4  small-up-4 large-up-4 small-centered columns"> 
                    Please enter your details to sign up :
                    <br><br>
                    <form id="signupForm" action="login/signup" method="post" >
                        <table>
                            <tr>
                                <td>Pseudo:</td>
                                <td><input id="pseudo" name="pseudo" type="text" size="16" value="<?= $pseudo ?>"></td>
                                <td class="errors" id="errPseudo"></td>
                            </tr>
                            <tr>
                                <td>Password:</td>
                                <td><input id="password" name="password" type="password" size="16"  value="<?= $password ?>" ></td>
                                <td class="errors" id="errPassword"></td>
                            </tr>
                            <tr>
                                <td>Confirm Password:</td>
                                <td><input id="password_confirm" name="password_confirm" size="16" type="password" value="<?= $password_confirm ?>" ></td>
                                <td class="errors" id="errPasswordConfirm"></td>
                            </tr>
                        </table>
                        <input id="btn" type="submit" value="Sign Up">
                    </form>

                </div>
            </div>
            <?php if (count($errors) != 0): ?>
                <div class='errors'>
                    <br><br><p>Please correct the following error(s) :</p>
                    <ul>
                        <?php foreach ($errors as $error): ?>
                            <li><?= $error ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>

        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/additional-methods.min.js"></script>
        <script>

            $("#signupForm").validate({
                rules: {
                    pseudo: {
                        required: true,
                        minlength: 3,
                        remote: {
                            url: "login/valid_pseudo",
                            type: "post"
                        }
                    },
                    password: {
                        required: true,
                        minlength: 3
                    },
                    password_confirm: {
                        required: true,
                        minlength: 3,
                        equalTo: "#password"
                    }
                },
                messages: {
                    pseudo: {
                        remote: jQuery.validator.format('Ce pseudo n\'est pas disponible')
                    }
                }
            });

        </script>

        <script src="lib/foundation-6/js/vendor/jquery.min.js"></script>
        <script src="lib/foundation-6/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>

    </body>
</html>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $user->pseudo ?>'s Profile!</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />

    </head>
    <body>        

        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>          

                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>

        <h4 class="title"><?= $user->pseudo ?>'s Profile!</h4> 


        <div class="row">
            <div class="medium-3  small-up-3 large-up-3 "> 



                <form id="editProfile" method='post' action='member/edit_users/<?= $user->id; ?>' >                   

                    Lastname : <input type="text" id="lastname" name="lastname" value = "<?= $user->lastname; ?>" />
                    Firstname : <input type="text" name="firstname" value = "<?= $user->firstname; ?>" />
                    Birthdate : <input type="date" name="birthdate" value = "<?= $user->birthdate; ?>" />
                    Email : <input type="text" name="email" value = "<?= $user->email; ?>" />
                    Phonenumber : <input type="text" name="phonenumber" value = "<?= $user->phonenumber; ?>" />


                    <?php
                    if ($member->isAdmin) {
                        $check = "";
                        $admin = "";
                        if ($user->enabled == 1) {
                            $check = " CHECKED ";
                        }
                        if ($user->isAdmin == 1) {
                            $admin = " CHECKED ";
                        }

                        echo 'Admin : <input type="checkbox" ' . $admin . ' name="isAdmin" /></br>';
                        echo 'enabled : <input type="checkbox" ' . $check . 'name="enabled"  />';
                    }
                    ?>
                    <input type='submit' value='Save User'>
                </form>


                <br>
                <br>

            </div>
        </div>

        <script src="lib/jquery-2.2.0.min.js"></script>         
        <script src="lib/foundation-6/js/foundation.min.js"></script>


        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/additional-methods.min.js"></script>
        <script>
            $("#editProfile").validate({
                rules: {
                    lastname: {
                        required: true,
                        minlength: 2
                    },
                    firstname: {
                        required: true,
                        minlength: 2
                    },
                    birthdate: {
                        required: true,
                        date: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 3
                    }
  
                }
            });
            $(document).foundation();
        </script>

    </body>
</html>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Produits</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />
    </head>
    <body>


        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li> 

                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>



        <h4 class="title">Categories</h4>   


        <?php
        ?>

        <div class="row">
            <div class="medium-8  small-up-4 large-up-6 "> 

                <table >
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th >Titre</th>
                            <th ></th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>


                        <?php
                        foreach ($categories as $val):
                            echo "<tr>";
                            echo "<td>" . $val->id . "</td>";
                            
                                                
                            echo "<td> <img  width='50px' src=".$val->photo.">  $val->titre  </td>";
                            echo "<td>" . $val->photo . "</td>";


                            if ($member->isAdmin) {
                                echo "<td>";
                                echo "<a href='categories/edit_categorie/$val->id' > <img src='view/img/edit.png' width='20'/></a>";
                                echo " : ";
                                echo "<a href='categories/delete_categorie/$val->id' > <img src='view/img/delete.png' width='20'/></a>";
                                 if ($val->enabled == 0) {
                                echo " : ";
                                echo "<a href='categories/enable_categorie/$val->id' > <img src='view/img/desable.png' title='Restaurer le produit' width='20'/></a>";
                            }
                            
                                echo "</td>";
                            } else {
                                echo "<td></td>";
                            }

                            echo "</tr>";

                        endforeach;
                        ?>

                    </tbody>
                </table>




                <br>
                <br>

            </div>
        </div>

        <script src="lib/foundation-6/js/vendor/jquery.min.js"></script>
        <script src="lib/foundation-6/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>
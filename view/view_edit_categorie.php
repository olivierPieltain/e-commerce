<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Edit categorie</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />
    </head>
    <body>
      
        
        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>                  
                </ul>
            </div>
            
            <div class="top-bar-right">
                <ul class="menu horizontale">
                <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>
        
         <div class="row"> 
         <div class="medium-3  small-up-3 large-up-3 "> 
            <?php if (count($categorie->id) == 0 ): ?>
                pas d'id spécifié !
            <?php else: ?>


                <form id="editCategorie" method='post' action='categories/update_categorie/<?= $categorie->id; ?>' enctype='multipart/form-data' >                   

                    ID : <?= $categorie->id; ?>
                    Titre : <input type="text" name="titre" value = "<?= $categorie->titre; ?>" required />
                    Photo : <input type="file" name="image" id="image" class="image" value = "" />
                    Enabled : <input type="checkbox" name="enabled"  <?php echo ($categorie->enabled == 1) ? 'checked' : 'tof'  ?>/>
               
                    
                    <input type='submit' value='Save Categorie'>
                </form>

                <?php if (strlen($success) != 0): ?>
                    <p><span class='success'><?= $success ?></span></p>
                <?php endif; ?>

                <?php if (strlen($error) != 0): ?>
                    <p><span class='errors'><?= $error ?></span></p>
                <?php endif; ?>  

            <?php endif; ?>            
            <br>
            <br>

        </div>
         </div>
        
        
        
        <script src="lib/jquery-2.2.0.min.js"></script>         
        <script src="lib/foundation-6/js/foundation.min.js"></script>
        
        
        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/additional-methods.min.js"></script>
        <script>
            $( "#editCategorie" ).validate({
                rules: {
                     titre: {
                         required: true,
                         minlength: 3
                     }//,
                     //photo: {
                     //    required: true,
                     //    url: true
                    // }             
  }
});
        
            
            
            $(document).foundation();
        </script>
    </body>
</html>
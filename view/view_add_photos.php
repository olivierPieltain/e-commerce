<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add photos</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />
        <script src="lib/jquery-2.2.0.min.js"></script>
        <link rel="stylesheet" type="text/css" href="lib/toastr.css"/>

    </head>
    <body>


        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>                
                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>

        <div class="row"> 
            <div class="medium-4  small-up-3 large-up-4 "> 

                </br>


                <form id="uploadform" method='post' action='produits/save_photo' >                   

                    <input type="hidden" name="id" id="id" value = "<?= $produit->id; ?>" />
                    Photo : <input type="file" name="image" id="image" class="image" value = "" />


                    <input class="button" type='submit' value='Add Photo'>
                </form>

            </div>
        </div>
        <div class="row text-center lightBkg">
            </br>
            <strong>Cliquer sur l'image pour l'effacer de la db </strong>
            </br>
        </div>

        <div id="photo" >

        </div>
        <script>



            $(document).ready(function () {
                getPhotos();
            });

            $('#photo').on('click', 'img', function () {
                var id = $(this).attr('id');
                var url = $(this).attr('class');
                alert(url);
                $.ajax({
                    url: 'produits/erase_photo',
                    method: "POST",
                    data: {id: id, url: url},
                    dataType: 'json'

                });
                $("#photo").empty();
                getPhotos();
                toastr["success"]('Photo erased succesfully');

            });
            $(document).ready(function (e) {
                $("#uploadform").on('submit', (function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: "produits/save_photo",
                        type: "POST",
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function ()
                        {
                            toastr["success"]("Photo added");
                            $("#photo").empty();
                            getPhotos();
                        },
                        error: function ()
                        {
                            toastr["fail"]("Error on upload");
                        }
                    });
                }));
            });

            function getPhotos() {
                var id = $("#id").val();
                var url;

                $.ajax({
                    url: 'produits/getPhotosAPI/' + id,
                    data: "",
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        for (var i = 0; i < data.length; i++) {
                            url = JSON.stringify(data[i].url);
                            var elem = document.createElement("img");

                            elem.width = 100;
                            elem.height = 75;
                            elem.style.margin = "30px";
                            elem.id = data[i].id;
                            elem.className = data[i].url;
                            elem.setAttribute("src", url.replace(/"/g, " "));
                            document.getElementById('photo').appendChild(elem);
                        }
                    }
                });
            }

        </script>


        <script src="lib/jquery-2.2.0.min.js"></script>         
        <script src="lib/foundation-6/js/foundation.min.js"></script>


        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/additional-methods.min.js"></script>
        <script>




            $(document).foundation();
        </script>
        <script src="lib/toastr.js"></script>
    </body>
</html>
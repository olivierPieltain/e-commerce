<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $member->pseudo ?>'s Profile!</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />

    </head>
    <body>        

        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>                    

                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>

        <h4 class="title"><?= $member->pseudo ?>'s Profile!</h4> 


        <div class="row">
            <div class="medium-3  small-up-3 large-up-3 "> 
                <?php if (count($member->pseudo) == 0): ?>
                    No profile string entered yet!
                <?php else: ?>


                    <form id="editProfile" method='post' action='member/edit_profile' >                   

                        Lastname : <input type="text" id="lastname" name="lastname" value = "<?= $member->lastname; ?>" />
                        Firstname : <input type="text" name="firstname" value = "<?= $member->firstname; ?>" />
                        Birthdate : <input type="date" name="birthdate" value = "<?= $member->birthdate; ?>" />
                        Email : <input type="text" name="email" value = "<?= $member->email; ?>" />
                        Phonenumber : <input type="text" name="phonenumber" value = "<?= $member->phonenumber; ?>" />
                        Password : <input type="password"id="password" name="password" value = "<?= $member->password; ?>" />
                        Confirm Password : <input type="password"id="password" name="password_confirm" value = "<?= $member->password; ?>" />

                        <?php
                        if ($member->isAdmin) {
                            $check = "";
                            $admin = "";
                            if ($member->enabled == 1) {
                                $check = " CHECKED ";
                            }
                            if ($member->isAdmin == 1) {
                                $admin = " CHECKED ";
                            }

                            echo 'Admin : <input type="checkbox" ' . $admin . ' name="isAdmin" /></br>';
                            echo 'enabled : <input type="checkbox" ' . $check . ' name="enabled"  />';
                        }
                        ?>
                        <input type='submit' value='Save Profile'>
                    </form>

                    <?php if (strlen($success) != 0): ?>
                        <p><span class='success'><?= $success ?></span></p>
                    <?php endif; ?>

                    <?php if (strlen($error) != 0): ?>
                        <p><span class='errors'><?= $error ?></span></p>
                        <?php endif; ?>  

                <?php endif; ?>            
                <br>
                <br>

            </div>
        </div>

        <script src="lib/jquery-2.2.0.min.js"></script>         
        <script src="lib/foundation-6/js/foundation.min.js"></script>


        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/additional-methods.min.js"></script>
        <script>
            $("#editProfile").validate({
                rules: {
                    lastname: {
                        required: true,
                        minlength: 2
                    },
                    firstname: {
                        required: true,
                        minlength: 2
                    },
                    birthdate: {
                        required: true,
                        date: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 3
                    },
                    password_confirm: {
                        required: true,
                        minlength: 3,
                        equalTo: "#password"
                    },
                    phonenumber: {
                        minlength: 5,
                        min: 0
                    }
                }
            });
            $(document).foundation();
        </script>

    </body>
</html>
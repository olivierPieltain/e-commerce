<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Produits</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />
        <link rel="stylesheet" type="text/css" href="lib/index.css"/>



    </head>
    <body>



        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>                 

                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>



        <h4 class="title">Create a new product</h4>   

        <div class="row">


            <form id="formCreateProduit" method='post' action='produits/create_produit' >                   


                Libellé : <input type="text" name="libelle" value = "" />
                Descriptif : <input type="text" name="descriptif" value = ""  />
                Prix : <input type="text" name="prix" value = "" />
                Quantité en stock : <input type="text" name="qtstock"  value = "" />


                <p>Choix de la Catégorie:</p>   


                <div class="row">

                    <?php
                    //print_r($catprod);

                    foreach ($categorylist as $category) {

                        //echo '<div class="small-6 medium-4 large-3 columns" ><input type="checkbox" ' . $check . 'name="categoryName[]" value="' . $category->id . '" /> ' . $category->titre . '</div>';
                        //echo '<div class="small-6 medium-4 large-3 columns" ><input type="checkbox" name="categoryName[]" value="' . $category->id . '" /> ' . $category->titre . '</div>';
                        echo '<div class="small-6 medium-4 large-3 columns" ><input type="checkbox" name="categoryName[]" value="' . $category->id . '" /> ' . $category->titre . '</div>';
                    }
                    ?>



                </div>


                <br>
                <input type='submit' value='Save Produit'>
            </form>

            <?php if (strlen($success) != 0): ?>
                <p><span class='success'><?= $success ?></span></p>
            <?php endif; ?>

            <?php if (strlen($error) != 0): ?>
                <p><span class='errors'><?= $error ?></span></p>
            <?php endif; ?>  

            <br />
            <form id="createCat" method="post"  action="categories/create_category">
                <input type="submit" value="Create new category"  />
            </form>



            <br>
            <br>

        </div>


        <script src="lib/jquery-2.2.0.min.js"></script>         
        <script src="lib/foundation-6/js/foundation.min.js"></script>     
        <script>
            $(document).foundation();
        </script>
        <script src="lib/jquery-2.2.0.min.js"></script>
        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/additional-methods.min.js"></script>
        <script>
            $("#formCreateProduit").validate({
                rules: {
                    libelle: {
                        required: true,
                        minlength: 3,
                        remote: {
                            url: "produits/valid_produit",
                            type: "post"
                        }
                    },
                    descriptif: {
                        required: true,
                        //minlength: 5
                    },
                    prix: {
                        required: true,
                        min: 0
                    },
                    qtstock: {
                        required: true,
                        min: 0
                    }

                },
                messages: {
                    libelle: {
                        remote: jQuery.validator.format('Ce nom de produit existe deja')                        
                    }                   
                },
            });

        </script>
    </body>
</html>
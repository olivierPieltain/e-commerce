<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>View error</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />
        <link rel="stylesheet" type="text/css" href="lib/index.css"/>

    </head>
    <body>

        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>          
                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>


        <div id="errors" data-alert class="callout alert round">

            
            <?php
            if ($error) {
                echo $error;
            }
            ?>
        </div>

        <div class="row"> 



        </div>



        <script src="lib/jquery-2.2.0.min.js"></script>         
        <script src="lib/foundation-6/js/foundation.min.js"></script>



        <script src="lib/additional-methods.min.js"></script>
        <script>

            $(document).foundation();

            $(document).ready(function () {

                $("#errors").show()

            });

        </script>


    </body>
</html>
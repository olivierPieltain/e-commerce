<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>New Password</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
        <script src="lib/jquery-2.2.0.min.js" type="text/javascript"></script>
        <script src="lib/toastr.js" type="text/javascript"></script>
        <link rel="stylesheet" href="lib/toastr.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />
        <link rel="stylesheet" type="text/css" href="lib/toastr.css"/>
        <style>
            h4{
                margin-left:20px;
            }
            h5{
                margin-left:20px;
            }
        </style>
        <script>
            $(function () {
                $("input:text:first").focus();
            });
        </script>
    </head>
    <body>

        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>
                    <li><a href="produits">Rechercher</a></li>                
                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>


        <h4 class="title">Réinitialisation E-Mail</h4>
        <h5> Un Email avec les instructions vous sera envoyé suite au remplissage du présent formulaire </h5>


        <div class="row"> 
            
 <div class="medium-4  small-up-4 large-up-4 small-centered columns"> 
                <form id="target"  action="login/valid_email" method="post">
                    <table>
                        <tr>
                            <td>Pseudo:</td>
                            <td><input id="pseudo" name="pseudo" type="text" value="" ></td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td><input id="email" name="email" type="email" value="" ></td>
                        </tr>
                    </table>
                    <input class="button"  type="submit" style="float:left;" value="Send">
                </form>


            </div>
        </div>
        <script src="lib/foundation-6/js/vendor/jquery.min.js"></script>
        <script src="lib/foundation-6/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>

        <script src="lib/jquery.validate.min.js"></script>
        <script src="lib/additional-methods.min.js"></script>
        <script>
            $("#target").validate({
                rules: {
                    pseudo: {
                        
                        
                    },
                    email: {
                        required: true,
                        minlength: 3,
                        remote: {
                            url: "login/valid_email",
                            type: "post"
                        }
                    }
                },
                messages: {
                    email: {
                        remote: jQuery.validator.format('Cet email n\'existe pas')
                    }
                }
            });


            $(".button").click(function (event) {
                event.preventDefault();

                var pseudo = $("#pseudo").val();
                var email = $("#email").val();
                $.ajax({
                    url: 'login/email_password',
                    method: "POST",
                    data: {pseudo: pseudo, email: email},
                    dataType: 'json'
                    
                });
                toastr["success"]("Email sended");
            });

        </script>

        <script src="lib/toastr.js"></script>


    </body>
</html>

<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <base href="<?= $web_root ?>"/>
        <title>Welcome to ITConsulting</title>

        <link rel="stylesheet" href="lib/foundation-6/css/foundation.css" />
        <link rel="stylesheet" href="lib/foundation-6/css/app.css" />

        <link rel="stylesheet" type="text/css" href="lib/DataTables/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" href="lib/toastr.css"/>

        <script src="lib/jquery-2.2.0.min.js"></script>
        <link rel="stylesheet" type="text/css" href="lib/index.css"/>


    </head>
    <body>

        <div class="top-bar">
            <div class="top-bar-left">                
                <ul class="menu">
                    <li class="menu-text">ITConsulting</li>                   
                    <li><a href="produits">Rechercher</a></li>                    
                </ul>
            </div>

            <div class="top-bar-right">
                <ul class="menu horizontale">
                    <?php include('menu.php'); ?> 
                </ul>
            </div>
        </div>





        <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
            <ul class="orbit-container">
                <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span>&#9664;</button>
                <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span>&#9654;</button>


                <li class="orbit-slide is-active">
                    <img src="view/img/banner3.png">
                </li>
                <li class="orbit-slide">
                    <img src="view/img/banner1.png">
                </li>
                <li class="orbit-slide">
                    <img src="view/img/banner2.png">
                </li>

            </ul>
        </div>


        <div id="errors" data-alert class="callout alert round">
            <?php
            if ($error) {

                if ($error) {
                    echo '<script>';
                    echo '$("#errors").show()';
                    echo '</script>';
                }
                echo $error;
            }
            ?>
        </div>


        <div class="row column text-center">
            <h3>Nos Categories de cours en pdf</h3>
            <hr>
        </div>

        <div id="nosCat" class="medium-6  small-up-2 large-up-6  text-center column  text-left lightBkg">
            
            <a href="" >Toutes nos catégorie.</a>
            </br></br>
            <?php
            foreach ($categories as $val):
                echo '<a href="home/indexByCat/' . $val->id . '">';
                echo '<img id="thumbnailcat" class="thumbnail" width="70"  hspace="5"  title="' . $val->titre . '"' . '   src="' . $val->photo . '">';
                echo '</a>';
            endforeach;
            ?>



        </div>
        <hr>



        <div class="large-12  text-center">   
            <h3>Nos supports de cours</h3>
        </div> 

        <hr>


        <div class="row">

            <div class="medium-8  small-up-2 large-up-4  text-center column">


                <?php
                foreach ($produits as $prod):
                    echo '<div  class="column" >';
                    if (array_key_exists(0, $prod->photos)) {
                        echo '<a class="thumbnail" href="produits/product_details/' . $prod->id . '"><img style="width: 100px; height:100px;"  src="' . $prod->photos[0]->url . '" alt="RSS"  /></a>';
                    } else {
                        echo '<a class="thumbnail" href="produits/product_details/' . $prod->id . '"><img style="width: 100px; height:100px;"  src="uploads/prod/pdf.png" alt="RSS"  /></a>';
                    }
                    echo '<p>' . $prod->libelle . '</p>';
                    echo '<p>€ ' . $prod->prix . '</p>';
                    echo '<a id="' . $prod->id . '"  class="button round" href="panier/ajouterArticle/' . $prod->id . '">Ajouter au panier</a>';

                    echo '</div>';
                endforeach;
                ?>

                <div  id="monPanier">




                </div>
            </div>
















            <hr>



            <div class="row column">
                <div class="callout primary text-center">
                    <h3>-10% pour les fêtes de Carnaval !</h3>
                </div>
            </div>
            <hr>




            <div class="row">
                <div class="medium-8  small-up-2 large-up-4  text-center column  callout secondary">


                    <?php
                    echo '<h4>Top 5 des produits</h4>';
                    echo '<br>';
                    $count = 0;




                    foreach ($produits as $prod):
                        echo '<div  class="column" >';
                        if (array_key_exists(0, $prod->photos)) {
                            echo '<a class="thumbnail" href="produits/product_details/' . $prod->id . '"><img style="width: 100px; height:100px;"  src="' . $prod->photos[0]->url . '" alt="RSS"  /></a>';
                        } else {
                            echo '<a class="thumbnail" href="produits/product_details/' . $prod->id . '"><img style="width: 100px; height:100px;"  src="uploads/prod/pdf.png" alt="RSS"  /></a>';
                        }
                        echo '<p>' . $prod->libelle . '</p>';
                        echo '<p>€ ' . $prod->prix . '</p>';
                        echo '<a id="' . $prod->id . '"  class="button round" href="panier/ajouterArticle/' . $prod->id . '">Ajouter au panier</a>';

                        echo '</div>';

                        $count++;
                        if ($count == 5)
                            break;
                    endforeach;
                    ?>


                </div>
            </div>


        </div>   









        <script src="lib/foundation-6/js/vendor/jquery.min.js"></script>
        <script src="lib/foundation-6/js/foundation.min.js"></script>


        <script>
            $(document).foundation();
        </script>
        <script>

            function generateTable() {
                $.ajax({
                    url: 'panier/getData',
                    data: "",
                    dataType: 'json',
                    success: function (data) {

                        //thead
                        var tablearea = document.getElementById('monPanier');
                        table = document.createElement('table');
                        table.id = "basket";
                        table.style = "width: 400px";
                        var header = table.createTHead();

                        var header = header.insertRow(0);
                        header.insertCell(0);
                        var headerlibelle = header.insertCell(0);

                        var headerquantite = header.insertCell(0);
                        var headerprixUnitaire = header.insertCell(0);

                        headerlibelle.innerHTML = "<b>Prix </b>";
                        headerquantite.innerHTML = "<b>Quantite</b>";
                        headerprixUnitaire.innerHTML = "<b>Libelle</b>";
                        headerprixUnitaire.style.textAlign = "center";
                        headerquantite.style.textAlign = "center";
                        headerlibelle.style.textAlign = "center";

                        //thead
                        //tbody
                        var tBody = document.createElement("tbody");
                        table.appendChild(tBody);
                        for (var i = 0; i < data.length; i++) {


                            var prix = JSON.stringify(data[i].prix);
                            var idprod = JSON.stringify(data[i].idproduit);
                            var libelle = JSON.stringify(data[i].libelle);
                            var qteProduit = JSON.stringify(data[i].qteProduit);
                            var tr = document.createElement('tr');
                            tr.id = "lignePanier";
                            tBody.appendChild(tr);
                            tr.appendChild(document.createElement('td'));
                            tr.appendChild(document.createElement('td'));
                            tr.appendChild(document.createElement('td'));
                            tr.appendChild(document.createElement('td'));

                            tr.cells[0].appendChild(document.createTextNode(libelle.replace(/"/g, " ")));
                            var input = document.createElement('input');
                            tr.cells[1].appendChild(input);
                            input.id = data[i].idproduit;
                            input.type = "text";
                            input.name = "quantity";
                            input.className = "quantity";
                            input.value = qteProduit.replace(/"/g, " ");
                            input.style.width = "50px";
                            tr.cells[2].appendChild(document.createTextNode(prix.replace(/"/g, " ")));
                            var supprimer = document.createElement('a');
                            var poubelle = document.createElement('IMG');
                            poubelle.src = "view/img/delete.png";
                            poubelle.title = "supprimer";
                            supprimer.appendChild(poubelle);
                            supprimer.name = "supprimer";
                            supprimer.className = "supprimer";
                            poubelle.width = 30;

                            supprimer.id = data[i].idproduit;
                            supprimer.href = "panier/supprimerArticle/" + idprod.replace(/"/g, "");
                            tr.cells[3].appendChild(supprimer);


                            table.appendChild(tr);

                        }
                        //tbody
                        var footer = table.createTFoot();
                        var row = footer.insertRow(0);
                        var cell3 = row.insertCell(0);

                        var cell1 = row.insertCell(0);

                        cell3.id = "total";

                        cell1.innerHTML = "Total";
                        cell3.style = "text-align:center;border:0px;";
                        cell1.style = "text-align:center;border:0px;";
                        var cell2 = row.insertCell(1);
                        var cell4 = row.insertCell(3);


                        tablearea.appendChild(table);





                        $(".supprimer").click(function (event) {
                            event.preventDefault();
                            var idprod = $(this).attr('id');
                            $.ajax({
                                url: 'panier/supprimerArticle',
                                method: "POST",
                                data: {idprod: idprod},
                                dataType: 'json',
                                complete: function () {
                                    $("#monPanier").empty();
                                    generateTable();
                                    getTotalPrice();
                                    toastr["success"]("Product deleted");
                                }

                            });

                        });
                        $(".quantity").change(function () {
                            var qty = $(this).val();
                            var idprod = parseInt($(this).attr('id'));
                            $.ajax({
                                url: 'panier/updateQTeArticle',
                                method: "POST",
                                data: {idprod: idprod, qty: qty},
                                dataType: 'json',
                                complete: function () {
                                    getTotalPrice();
                                    toastr["success"]("Quantity successfully updated");
                                }

                            });

                        });
                    }
                });
            }
            $(".button").click(function (event) {
                event.preventDefault();
                sessionCheck();
                var idprod = $(this).attr('id');
                $.ajax({
                    url: 'panier/ajouterArticle',
                    method: "POST",
                    data: {idprod: idprod},
                    dataType: 'json',
                    complete: function () {

                        $("#monPanier").empty();
                        generateTable();
                        getTotalPrice();
                    }
                });




            });
            $(document).ready(function () {
                generateTable();
                getTotalPrice();


            });
            function getTotalPrice() {
                $.ajax({
                    url: 'panier/getPrice',
                    data: "",
                    dataType: 'json',
                    success: function (data) {

                        document.getElementById("total").innerHTML = data;


                    }
                });
            }
            function sessionCheck() {

                $.ajax({
                    url: 'panier/sessionCheck',
                    data: "",
                    dataType: 'json',
                    success: function (data) {
                        $("#panier").html('Panier');
                        toastr["success"]("Product added");
                    },
                    error: function (data) {
                        toastr["error"]('Connectez-vous pour ajouter au panier');
                    }



                });

            }



        </script>

    </body>
    <script src="lib/toastr.js"></script>
</html>

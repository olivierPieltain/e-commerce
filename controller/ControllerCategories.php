<?php

require_once 'model/Categories.php';
require_once 'framework/View.php';
require_once 'MyController.php';

class ControllerCategories extends MyController {

    public function create_category() {
        $member = $this->get_user_or_redirect();
        $error = "";
        $success = "";
        if ($member->isAdmin) {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
            } else {
                $error = "Error : id introuvable";
            }
        }
        if (isset($_POST['titre']) && isset($_FILES['image'])) {
            Categories::add_categoriebyvalues($_POST['titre'], "", 1);
            $categorie = Categories::get_categorieByTitre($_POST['titre']);
            $img = $_FILES['image']['name'];
            $tmp = $_FILES['image']['tmp_name'];
            self::save_photo($categorie, $img, $tmp);
        }

        (new View("create_category"))->show(array("member" => $member));
    }

    public function save_photo($categorie, $img, $tmp) {

        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp');
        $path = 'uploads/cat/' . $categorie->id . '/';
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }


        $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
        $final_image = rand(1000, 1000000) . $img;
        if (in_array($ext, $valid_extensions)) {
            $path = $path . strtolower($final_image);

            if (move_uploaded_file($tmp, $path)) {

                $categorie->photo = $path;
                Categories::update_categorie($categorie);
            }
        } else {
            echo 'invalid file';
        }
    }

    public function valid_categorie() {
        $titre = $_POST['titre'];
        if (Categories::check_categorie_name($titre)) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function edit_categorie() {
        $member = $this->get_user_or_redirect();
        $error = "";
        $success = "";
        $id = $_GET['id'];
        $error = self::getUrlError($id);
        if ($error) {
            (new View("error"))->show(array("member" => $member, "error" => $error));
        } else if ($member->isAdmin) {
            $categorie = Categories::get_categorie($id);
            (new View("edit_categorie"))->show(array("error" => $error, "categorie" => $categorie, "member" => $member, "error" => $error, "success" => $success));
            $error = "";
        }
    }

//gestion de l'édition du profil
    public function update_categorie() {
        $member = $this->get_user_or_redirect();
        $error = "";
        $success = "";
        if ($member->isAdmin) {
            $id = $_GET['id'];

            if (isset($_POST['titre'])) {
                $titre = $_POST['titre'];
                $img = $_FILES['image']['name'];
                $tmp = $_FILES['image']['tmp_name'];

                if (isset($_POST['enabled'])) {
                    $enabled = 1;
                } else {
                    $enabled = 0;
                }


                $categorie = Categories::get_categorie($id);

                $categorie->titre = $titre;
                $categorie->enabled = $enabled;
                self::save_photo($categorie, $img, $tmp);
                $success = "Your category has been successfully updated.";
            }
        }
        //$this->showCategories();
        $categories = Categories::get_categories();
        (new View("categories"))->show(array("categories" => $categories, "member" => $member));
    }

//page d'accueil. 
    public function index() {
        $this->showCategories();
    }

    //liste des produits.
    public function showCategories() {
        //$member = $this->get_user_or_redirect();
        if (!isset($_GET["id"]) || $_GET["id"] == "") {
            $member = $this->get_user_or_redirect();
        } else {
            $member = Member::get_member($_GET["id"]);
        }

        $categories = Categories::get_ALLcategories();
        (new View("categories"))->show(array("categories" => $categories, "member" => $member));
    }

    public function delete_categorie() {

        $member = $this->get_user_or_redirect();

        $success = "";
        $id = $_GET['id'];
        $error = self::getUrlError($id);
        if ($error) {
            (new View("error"))->show(array("member" => $member, "error" => $error));
        } else if ($member->isAdmin) {

            Categories::delete_categorie($id);
            $categories = Categories::get_ALLcategories();
            (new View("categories"))->show(array("categories" => $categories, "member" => $member));
        }
    }

    public function enable_categorie() {

        $member = $this->get_user_or_redirect();
        $success = "";
        $id = $_GET['id'];
        $error = self::getUrlError($id);

        if ($error) {
            (new View("error"))->show(array("member" => $member, "error" => $error));
        } else if ($member->isAdmin) {
            Categories::enable_categorie($id);
            $categories = Categories::get_ALLcategories();
            (new View("categories"))->show(array("categories" => $categories, "member" => $member));
        }
    }

}

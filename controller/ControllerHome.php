<?php

require_once 'model/Panier.php';
require_once 'model/Produits.php';
require_once 'model/Categories.php';
require_once 'framework/View.php';
require_once 'MyController.php';

class ControllerHome extends MyController {

//page d'accueil. 
    public function index() {
        $this->showHome();
    }

    //liste des produits.
    public function showHome() {
        //$panier   = new Panier("karkar", 7, "Myproduit", 5,99);
        $panier = null;
        $error = "";

        if ($this->user_logged()) {
            $member = $this->get_user_or_redirect();
        } else {
            $member = new Member(0, "guest", "guest");
        }

        $categories = Categories::get_categories();
        $produits = Produits::get_produits();
        (new View("index"))->show(array("error" => $error, "categories" => $categories, "member" => $member, "produits" => $produits, "panier" => $panier));
    }

    //Question: Deux parametres get avec le meme nom (member et category id), comment faire?
    public function indexByCat() {


        $member = $this->get_user_or_redirect();

        $id = $_GET['id'];
        $error = self::getUrlError($id);

        $produits = Produits::get_produitsByCategorie($id);
        //(new View("produits"))->show(array("produits" => $produits, "member" => $member));        
        $panier = null;
        $categories = Categories::get_categories();
        (new View("index"))->show(array("error" => $error, "categories" => $categories, "member" => $member, "produits" => $produits, "panier" => $panier));
    }

}

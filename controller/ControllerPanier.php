<?php

require_once 'controller/ControllerHome.php';
require_once 'model/Panier.php';
require_once 'model/Member.php';
require_once 'model/Produits.php';
require_once 'framework/View.php';
require_once 'MyController.php';

class ControllerPanier extends MyController {

    public function index() {
        if (!isset($_GET["id"]) || $_GET["id"] == "") {
            $member = $this->get_user_or_redirect();
        } else {
            $member = Member::get_member($_GET["id"]);
        }
        $produits = Produits::get_produits();
        (new View("test"))->show(array("produits" => $produits, "member" => $member));
    }

    public function getPrice() {
        if (!isset($_GET["id"]) || $_GET["id"] == "") {
            $member = $this->get_user_or_redirect();
        } else {
            $member = Member::get_member($_GET["id"]);
        }
        $total = Panier::getTotal($member->pseudo);


        echo (json_encode($total));
    }

    //liste des produits.
    public function getData() {
        //$member = $this->get_user_or_redirect();
        if (!isset($_GET["id"]) || $_GET["id"] == "") {
            $member = $this->get_user_or_redirect();
        } else {
            $member = Member::get_member($_GET["id"]);
        }
        $panier = Panier::getPanierForApi($member->pseudo);

        echo (json_encode($panier));
    }

    public function sessionCheck() {
        $member = $this->get_user_or_redirect();
        $pseudo = $member->pseudo;
        echo(json_encode($pseudo));
    }

    public function fastLogin() {
        $pseudo = $_POST["pseudo"];
        $password = $_POST["password"];
        $member = Member::get_member($pseudo);
        if ($member) {
            if ($this->check_password($password, $member->password)) {
                $this->log_user($member);
            }
        }
    }

    public function supprimerArticle() {
        $idprod = $_POST["idprod"];

        $member = $this->get_user_or_redirect();
        Panier::removeProduits($member->pseudo, $idprod);
    }

    public function ajouterArticle() {
        $idprod = $_POST["idprod"];

        if (!isset($member)) {
            $member = $this->get_user_or_redirect();
        }
        $panier = Panier::getPanier($member->pseudo);
        if (isset($idprod)) {
            $produit = Produits::get_produit($idprod);
            $panier = Panier::getPanier($member->pseudo);
        }
        $count = 0;
        foreach ($panier as $val) {
            if ($val->idproduit == $idprod) {
                $count++;
            }
        }

        Panier::addProduit($member->pseudo, $idprod, $count + 1);
        echo "ok";
    }

    public function updateQTeArticle() {
        $idprod = $_POST["idprod"];
        $qty = $_POST["qty"];
        $member = $this->get_user_or_redirect();
        Panier::updateQTeArticle($idprod, $member->pseudo, $qty);
    }

}

?>
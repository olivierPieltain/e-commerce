<?php

require_once 'framework/Controller.php';

abstract class MyController extends Controller {


    public static function getUrlError($id) {
         $err="";
        if (!isset($id) || $id == "") {
            $err = "Error : l'id est introuvable";            
        } else {

            if (!is_numeric($id)) {
                $err = "Error : l'id est invalide";             
            }
        }
        return $err;
    }

}
?>



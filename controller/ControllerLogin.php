<?php

require_once 'model/Member.php';
require_once 'model/Produits.php';
require_once 'model/Categories.php';
require_once 'framework/View.php';
require_once 'MyController.php';
require_once 'lib/PHPMailer/class.phpmailer.php';
require_once 'lib/PHPMailer/class.smtp.php';


$pseudo = '';
$password = '';
$password_confirm = '';
$errors = [];

class ControllerLogin extends MyController {

    //si l'utilisateur est conecté, redirige vers son profil.
    //sinon, produit la vue d'accueil.
    public function index() {
        if ($this->user_logged()) {
            $this->redirect("member", "index");
        } else {
            $this->login();
        }
    }

    //gestion de la connextion d'un utilisateur
    public function login() {


        if (!isset($member)) {
            $member = new Member(0, "guest", "guest");
        }


        $pseudo = '';
        $password = '';
        $error = '';
        if (isset($_POST['pseudo']) && isset($_POST['password'])) { //note : pourraient contenir
            //des chaînes vides
            $pseudo = $_POST['pseudo'];
            $password = $_POST['password'];

            $member = Member::get_member_by_pseudo($pseudo);
            if ($member) {
                if ($this->check_password($password, $member->password)) {
                    $this->log_user($member);
                } else {
                    $error = "Wrong password. Please try again.";
                }
            } else {
                $error = "Can't find a member with the pseudo '$pseudo'. Please sign up.";
            }
        }

        (new View("login"))->show(array("pseudo" => $pseudo, "password" => $password, "member" => $member, "error" => $error));
    }

    //affiche le formulaire permettant l'envoi du mail
    public function passwordRetrieve() {
        $member = new Member(0, "guest", "guest");
        (new View("password_retrieve"))->show(array("member" => $member));
    }

    //affiche le formulaire suite au redirect du mail
    public function password_define() {
        $id = $_GET['id'];
        $member = Member::get_member($id);
        (new View("define_password"))->show(array("member" => $member));
    }

    //change le mot de passe
    public function password_changing() {
        $password = $_POST['password'];
        $password_confirm = $_POST['password_confirm'];
        if ($password === $password_confirm) {
            $id = $_POST['id'];
            $pseudo = $_POST['pseudo'];
        }
        if (Member::check_pseudo_with_id($id, $pseudo)) {
            $member = Member::get_member($id);
            $member->password = $this->my_hash($password);
            Member::update_member($member);
            echo (json_encode("ok"));
        } else {
            echo (json_encode("ko"));
        }
    }

    /////////////////////
    public function exists_service() {

        $res = "false";
        if (isset($_GET["id"]) && $_GET["id"] !== "") {
            $member = Member::get_member($_GET["id"]);
            if ($member)
                $res = "true";
        }
        echo $res;
    }

    public function email_password() {
        $pseudo = $_POST["pseudo"];
        $email = $_POST["email"];
        //TODO   find and check if person exists in db, if yes send mail and handle ajax success, otherwise show nice error message


        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP               // enables SMTP debug information (for testing)
        $mail->SMTPAuth = true;
        // enable SMTP authentication
        $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
        $mail->SMTPDebug = 2;
        $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
        $mail->Port = 587;                   // set the SMTP port for the GMAIL server
        $mail->Username = "itconsultingbruxelles@gmail.com";  // GMAIL username
        $mail->Password = "motdepasse123";            // GMAIL password
        $mail->SetFrom('itconsultingbruxelles@gmail.com', 'admin');
        $mail->Subject = "Retrieve your password";
        $mail->AltBody = "refresh"; // optional, comment out and test
        $mail->IsHTML(true);
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $member = Member::get_member_by_pseudo($pseudo);
        $mail->MsgHTML("http://127.0.0.1/prwb_groupe11/login/password_define/" . $member->id);
        $address = $email;
        $mail->AddAddress($address, "admin");
        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }
    }

    //renvoie un tableau de strings en fonction des erreurs de signup.
    public function valid_pseudo() {
        $pseudo = $_POST['pseudo'];
        if (Member::check_pseudo($pseudo)) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function valid_email() {
        $email = $_POST['email'];
        if (Member::check_email($email)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function valid_password() {
        $password = $_POST['password'];


        if (Member::check_password($this->my_hash($password))) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function signup() {
        if (!isset($member)) {
            $member = new Member(0, "guest", "guest");
        }
        $pseudo = "";
        $password = "";
        $password_confirm = "";
        $errors = [];
        if (isset($_POST['pseudo']) && isset($_POST['password']) && isset($_POST['password_confirm'])) {
            $pseudo = ($_POST['pseudo']);
            $password = $_POST['password'];
            $password_confirm = $_POST['password_confirm'];
            if ($password === $password_confirm) {
                $member = new Member(0, $pseudo, $this->my_hash($password));
                Member::add_member($member);
            }
            $member = Member::get_member_by_pseudo($pseudo);
                $this->log_user($member);
            
        }

        (new View("signup"))->show(array("member" => $member, "pseudo" => $pseudo, "password" => $password, "password_confirm" => $password_confirm, "errors" => $errors));
    }

}

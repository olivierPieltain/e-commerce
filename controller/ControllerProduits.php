<?php

require_once 'model/Produits.php';
require_once 'model/Categories.php';
require_once 'framework/View.php';
require_once 'MyController.php';

class ControllerProduits extends MyController {

    public function create_produit() {
        $member = $this->get_user_or_redirect();
        $error = "";
        $success = "";
        $libelle = "";
        $descriptif = "";
        $prix = "";
        $qtstock = "";
        $categoryName = array();


        if (isset($_POST['libelle']) && isset($_POST['descriptif']) && isset($_POST['prix']) && isset($_POST['qtstock'])) {
            $libelle = $_POST['libelle'];
            $descriptif = $_POST['descriptif'];
            $prix = $_POST['prix'];
            $qtstock = $_POST['qtstock'];
            $enabled = true;
            if (isset($_POST['categoryName'])) {
                $categoryName = $_POST['categoryName'];
            }

            if (Produits::get_produitbylibelle($libelle)) {
                $error = "Ce nom de produit existe deja";
            } else {
                Produits::add_produitbyvalues($libelle, $descriptif, $prix, $qtstock, $enabled);
                $prod = Produits::get_produitbylibelle($libelle);
                foreach ($categoryName as $elem) {
                    Produits::add_category($elem, $prod->id);
                }
                $success = "produit a été rajouté.";
            }
        }

        $liste = Categories::get_categories();


        (new View("create_produit"))->show(array("categorylist" => $liste, "member" => $member, "libelle" => "$libelle", "descriptif" => $descriptif, "prix" => $prix, "qtstock" => $qtstock, "error" => $error, "success" => $success));
    }

    public function product_details() {
        $member = $this->get_user_or_redirect();
        $error = "";
        $id = $_GET['id'];
        $error = self::getUrlError($id);

        if ($error) {
            (new View("error"))->show(array("member" => $member, "error" => $error));
        } else {
            $categories = Categories::get_categories();
            $produit = Produits::get_produit($id);
            $produits = Produits::get_produits();
            (new View("product_detail"))->show(array("error" => $error, "produits" => $produits, "categories" => $categories, "produit" => $produit, "member" => $member));
        }
    }

    public function edit_produit() {
        $member = $this->get_user_or_redirect();
        $error = "";
        $success = "";

        $id = $_GET['id'];
        $error = self::getUrlError($id);

        if ($error) {
            (new View("error"))->show(array("member" => $member, "error" => $error));
        } else if ($member->isAdmin) {
            $produit = Produits::get_produit($id);
            $liste = Categories::get_categories();
            $catprod = Categories::get_categoriesById_prod($id);
            (new View("edit_produit"))->show(array("error" => $error, "produit" => $produit, "member" => $member, "categorylist" => $liste, "catprod" => $catprod, "error" => $error, "success" => $success));
        }
    }

    public function add_photos() {
        $member = $this->get_user_or_redirect();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        } else {
            $error = "Error : id introuvable";
        }
        if ($member->isAdmin) {
            $produit = Produits::get_produit($id);

            (new View("add_photos"))->show(array("produit" => $produit, "member" => $member));
        }
    }

    public function save_photo() {
        $id = $_POST['id'];
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp');
        $path = 'uploads/' . $id . '/';
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        if (isset($_FILES['image'])) {
            $img = $_FILES['image']['name'];
            $tmp = $_FILES['image']['tmp_name'];
            $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
            $final_image = rand(1000, 1000000) . $img;
            if (in_array($ext, $valid_extensions)) {
                $path = $path . strtolower($final_image);

                if (move_uploaded_file($tmp, $path)) {
                    echo "<img src='$path' />";
                    $photo = new Photo(0, $path, $id);
                    Photo::add_photo($photo);
                }
            } else {
                echo 'invalid file';
            }
        }
    }

    public function erase_photo() {
        $id = $_POST['id'];
        $url = $_POST['url'];
        if (file_exists($url)) {
            unlink($url);
        }
        Photo::delete_photo($url);
    }

//gestion de l'édition du profil
    public function update_produit() {
        $member = $this->get_user_or_redirect();
        $error = "";
        $success = "";

        if ($member->isAdmin) {
            $id = $_GET['id'];

            //if member is Admin!!!!
            if (isset($_POST['libelle']) && isset($_POST['descriptif']) && isset($_POST['prix']) && isset($_POST['qtstock'])) {
                $libelle = $_POST['libelle'];
                $descriptif = $_POST['descriptif'];
                $prix = $_POST['prix'];
                $qtstock = $_POST['qtstock'];
                $categoryName = $_POST['categoryName'];

                if (isset($_POST['enabled'])) {
                    $enabled = 1;
                } else {
                    $enabled = 0;
                }


                $produit = Produits::get_produit($id);

                $produit->libelle = $libelle;
                $produit->descriptif = $descriptif;
                $produit->prix = $prix;
                $produit->qtstock = $qtstock;
                $produit->enabled = $enabled;
                $produit->id = $id;
                Produits::update_produit($produit);



                Produits::remove_AllcategoriesFromProduit($id);

                foreach ($categoryName as $cat) {
                    Produits::add_category($cat, $id);
                }


                $success = "Your product has been successfully updated.";
            }
        } else {
            $error = "Seul l'admin peut modifier un produit";
        }

        $this->edit_produit();
    }

//page d'accueil. 
    public function index() {
        $this->showProduits();
    }

    //liste des produits.
    public function showProduits() {
        //$member = $this->get_user_or_redirect();
        if (!isset($_GET["id"]) || $_GET["id"] == "") {
            $member = $this->get_user_or_redirect();
        } else {
            $member = Member::get_member($_GET["id"]);
        }
        if ($member->isAdmin) {
            $produits = Produits::get_Allproduits();
        } else {
            $produits = Produits::get_produits();
        }
        (new View("produits"))->show(array("produits" => $produits, "member" => $member));
    }

    public function getPhotosAPI() {
        $id = $_GET['id'];
        $photos = Produits::get_photosByProduit($id);
        echo (json_encode($photos));
    }

    public function valid_produit() {
        $libelle = $_POST['libelle'];
        if (Produits::check_product_name($libelle)) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function delete_produit() {

        $member = $this->get_user_or_redirect();
        $success = "";
        $id = $_GET['id'];
        $error = self::getUrlError($id);
        if ($error) {
            (new View("error"))->show(array("member" => $member, "error" => $error));
        } else if ($member->isAdmin) {
            Produits::delete_produit($id);
            $produits = Produits::get_Allproduits();
            (new View("produits"))->show(array("produits" => $produits, "member" => $member));
        }
    }

    public function enable_produit() {

        $member = $this->get_user_or_redirect();
        $success = "";
        $id = $_GET['id'];
        $error = self::getUrlError($id);

        if ($error) {
            (new View("error"))->show(array("member" => $member, "error" => $error));
        } else if ($member->isAdmin) {
            Produits::enable_produit($id);
            $produits = Produits::get_produits();
            (new View("produits"))->show(array("produits" => $produits, "member" => $member));
        }
    }

}

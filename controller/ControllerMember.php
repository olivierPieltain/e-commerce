<?php

require_once 'model/Member.php';
require_once 'framework/View.php';
require_once 'MyController.php';

class ControllerMember extends MyController {

//gestion de l'édition du profil
    public function edit_profile() {
        $member = $this->get_user_or_redirect();
        $error = "";
        $success = "";


        if (isset($_POST['lastname']) && isset($_POST['firstname']) && isset($_POST['birthdate']) && isset($_POST['email']) && isset($_POST['phonenumber'])) {
            $lastname = $_POST['lastname'];
            $firstname = $_POST['firstname'];
            $birthdate = $_POST['birthdate'];
            $email = $_POST['email'];
            $phonenumber = $_POST['phonenumber'];

            $member->lastname = $lastname;
            $member->firstname = $firstname;
            $member->birthdate = $birthdate;
            $member->email = $email;
            $member->phonenumber = $phonenumber;
            if (isset($_POST['password'])) {
                $password = $_POST['password'];
                if(Member::check_hashedPassword($password)){                   
                }
                else{
                    $member->password= $this->my_hash($password);
                }
            }

            Member::update_member($member);
            $success = "Your profile has been successfully updated.";
        }
        (new View("profile"))->show(array("member" => $member, "error" => $error, "success" => $success));
    }

    public function edit_users() {
        $member = $this->get_user_or_redirect();
        $user = Member::get_member($_GET["id"]);
        $error = "";
        $success = "";

        if ($member->isAdmin) {
            if (isset($_POST['lastname']) && isset($_POST['firstname']) && isset($_POST['birthdate']) && isset($_POST['email']) && isset($_POST['phonenumber'])) {
                $lastname = $_POST['lastname'];
                $firstname = $_POST['firstname'];
                $birthdate = $_POST['birthdate'];
                $email = $_POST['email'];
                $phonenumber = $_POST['phonenumber'];


                if (isset($_POST['enabled'])) {
                    $enabled = 1;
                } else {
                    $enabled = 0;
                }

                if (isset($_POST['isAdmin'])) {
                    $isAdmin = 1;
                } else {
                    $isAdmin = 0;
                }

                $user->lastname = $lastname;
                $user->firstname = $firstname;
                $user->birthdate = $birthdate;
                $user->email = $email;
                $user->phonenumber = $phonenumber;
                $user->enabled = $enabled;
                $user->isAdmin = $isAdmin;

                Member::update_member($user);
                $success = "User has been successfully updated.";

                $this->redirect("member", "users");
            }
        }
        $error = "Seul l'admin peut faire cela";
        (new View("edit_user"))->show(array("member" => $member, "user" => $user, "error" => $error, "success" => $success));
    }

//page d'accueil. 
    public function index() {
        $this->profile();
    }

//profil de l'utilisateur connecté ou donné
    public function profile() {
        $error = "";
        $success = "";
        if (!isset($_GET["id"]) || $_GET["id"] == "") {
            $member = $this->get_user_or_redirect();
        } else {
            $member = Member::get_member($_GET["id"]);
        }
        (new View("profile"))->show(array("member" => $member, "error" => $error, "success" => $success));
    }

    //liste des membres.
    public function users() {
        if (!isset($_GET["id"]) || $_GET["id"] == "") {
            $member = $this->get_user_or_redirect();
        } else {
            $member = Member::get_member($_GET["id"]);
        }

        $members = $member->get_members();
        (new View("users"))->show(array("members" => $members, "member" => $member));
    }

    public function delete_users() {

        $member = $this->get_user_or_redirect();
        $error = "";
        $success = "";

        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        } else {
            $error = "Error : id introuvable";
        }

        Member::delete_member($id);


        $members = $member->get_members();
        (new View("users"))->show(array("members" => $members, "member" => $member));
    }

    public function enable_users() {

        $member = $this->get_user_or_redirect();
        $error = "";
        $success = "";

        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        } else {
            $error = "Error : id introuvable";
        }

        Member::enable_member($id);


        $members = $member->get_members();
        (new View("users"))->show(array("members" => $members, "member" => $member));
    }

}
